:: Change directory to the path of this batch file
cd %~dp0

:: Display the pm2 status
call node_modules\.bin\pm2 status

:: Pause to keep the command prompt open so we can see the status output
pause
