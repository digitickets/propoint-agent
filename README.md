# ProPoint Agent

This is an app that runs in Node.js on client devices to provide access to resources we'd otherwise be unable to use due to ProPoint running in the browser.
ProPoint in the browser makes a Websocket connection to the agent to communicate with it.

There is also an Electron-based version of ProPoint Agent in development here: https://bitbucket.org/digitickets/propoint-agent-electron/src
That application uses this repository as a dependency. Most of the logic still lives here and that application is mostly
just an Electron wrapper around it.

⚠⚠⚠ There is a branch called `no-phantom` which has phantomjs and the printing code removed. The Electron agent uses
that branch as a dependency. If you release an update to this repository, make sure you rebase and push the `no-phantom`
branch so that gets updated too.
Some day https://digitickets.atlassian.net/browse/PROP-322 will do away with printing via the ProPoint Agent and then
we can merge the `no-phantom` branch into the main ProPoint Agent code.



## Node Versions

In the wild (on client machines) it runs on a few different versions of Node so we must support all of these.
The lowest version being used as of 23/9/2024 is 10.15.0.
All versions in active use are as follows. Those marked with a (*) are used by lots of devices.
- 10.15.0
- 10.16.0
- 10.16.3 (*)
- 12.18.1 (*)
- 12.22.12
- 16.14.0
- 16.17.1 (*)
- 18.16.0
- 18.16.1
- 18.17.1
- 18.18.0
- 20.9.0
- 20.10.0

## Features

### Socket Proxy

Verifone, Worldpay, (and other brand) chip and pin terminals can be communicated with using a TCP/IP socket. The EPOS running in the browser can't directly connect to a TCP socket.

This is a small Node.js (javascript) app the runs on each client's computer. The EPOS connects to this using a WebSocket, sends the details of the TCP socket(s) it wishes to connect to, and this app forwards between the WebSocket and TCP socket.

The 'socket.io' package is used on both this server and in the client (EPOS) to run the Websocket.

## Releasing

This repository is cloned onto the server `fruitvale.dedi.melbourne.co.uk` in:
```
/var/www/vhosts/dtapps.co.uk/httpdocs/downloads/propoint-agent
```
and zipped up into 2 files:
```
/var/www/vhosts/dtapps.co.uk/httpdocs/downloads/propoint-agent.zip
/var/www/vhosts/dtapps.co.uk/httpdocs/downloads/socket-proxy.zip
```
The `socket-proxy.zip` file is for old clients updating to the renamed version for the first time. Once they are up
to date they will begin downloading the correctly named zip.

That zip file can be downloaded from: http://downloads.dtapps.co.uk

To install on the windows client you also need to install NodeJS version 6.11.2. This can be downloaded from: https://nodejs.org/download/release/v6.11.2/ (choose the x86 or x64 msi installer as appropriate). After node is installed move to the install directory and run the `Agent.bat` file.

To release the latest version:
```
ssh root@fruitvale.dedi.melbourne.co.uk
cd /var/www/vhosts/dtapps.co.uk/httpdocs/downloads/propoint-agent
sudo -u dtapps git pull
```
A post-checkout hook will run `scripts/create-zip.sh` to create the zip file.

### Auto Updates

Each night (at 1AM unless overridden) the code running on client devices automatically exits, downloads the latest version of the .zip, and restarts itself.

If you launch the app using the `Agent.bat` script it will run the `scripts/update-and-run.js` file, which will download that zip (overwriting the contents of the directory!) before running the server.

## Notes

### SSL

Because ProPoint uses SSL the Websocket this server exposes also needs to be secure.
Remember this server will be running locally on the client's device, so is accessible to ProPoint in the browser at 127.0.0.1

- A DNS record has been created for "localhost.dtapps.co.uk" which points to 127.0.0.1
- An SSL certificate has been created for "localhost.dtapps.co.uk"
- That certificate and its key are distributed with this app so it can run a secure Websocket server at https://localhost.dtapps.co.uk

#### Renewing The SSL Certificate

The certificate was purchased by Anthony from ssls.com on 2020-08-07 for the domain "localhost.dtapps.co.uk". It is a 4 year certificate BUT it needs to be renewed (recreated) every year. It has been renewed in 2021 and 2022 so can be renewed one more time in 2023.

MX records have been set up so Anthony can receive emails to @localhost.dtapps.co.uk for the purposes of verifying ownership of the domain for the certificate.

NOTE: When you update the certificate for ProPoint Agent, the same certificate should also go into ProPoint Printer as that also uses SSL at localhost.dtapps.co.uk


## Running In Development

Client devices are running NodeJS version 6.11.2. You can install this version alongside any other versions you may desire using `nvm`: https://www.wdiaz.org/how-to-install-nvm-with-homebrew/

To run the server in development:
```
npm start
```

**!! Do not use the `Agent.bat`, `Proxy.bar` or `scripts/update-and-run.js` scripts during development or your directory will be overwritten with the zip from the server !!**

Remember to stop (Ctrl+C) and start (`npm start`) the server when making any changes to code.

### Generate log files in devlopment

Running the server with `npm start` will not generate any log files as all the output will go to the console. To run agent more realistically and generate log files use pm2 as we do in production.

Create a copy of `config/pm2.json` e.g. `config/pm2-dev.json` and then edit as appropriate (for example call `scripts/run.js` instead of `scripts/update-and-run.js` which will download and overwrite your changes)

e.g. `./node_modules/.bin/pm2 start ./config/pm2-dev.json`

To stop you will then need to use `./node_modules/.bin/pm2 stop all` and to shut down pm2 `./node_modules/.bin/pm2 kill`

### Development Tips

You can run a dummy TCP socket server by running this command. It will print any data sent to it.
```
nc -lk [port]
```
e.g.
```
nc -lk 10000
```

https://www.hw-group.com/software/hercules-setup-utility (Windows only) is also a useful tool for testing a TCP server connection.

## Scripts

There are several .js and .bat files.

### Batch Scripts

#### `Agent.bat` (also `Proxy.bat` for backward compatibility)
Start the server with the pm2 process manager package. It will continue to run in the background and pm2 will ensure it automatically restarts if it stops. Note that this script also installs the necessary dependencies, so you need to run this one at least once before the other batch scripts will work.

#### `Logs.bat`
Use this to view output from the server.

#### `Restart.bat`
Use this to restart the server.

#### `Status.bat`
Use this to check if the server is running.

#### `Stop.bat`
You must use this to tell pm2 to stop running the server, then stop running pm2 itself.
Note that if you have the `Logs.bat` open you will need to close and reopen it after running this.

#### `scripts/run.bat`
Run once. This does not do any updating, so changes won't be lost. Use this to run the server for development or testing. This just runs `scripts.run.js` but it easier because you can double click it to run.

#### `scripts/install.bat`
Runs `npm install`

#### `scripts/test.bat`
Runs `npm run test`

### JS Scripts
These are the underlying .js files the .bat files are running.

####  `scripts/run.js`
Run with: `npm start`

Simply run the server once. Exit with Ctrl+C.

#### `scripts/update-and-run.js`
Downloads the latest version of this app from the URL defined in `config/update.json` then runs `scripts/run.js`. This is the script being run by 'pm2' when you use `Proxy.bat` or `Agent.bat`.

## Configuration

You can set any configuration options in config/config.json (see config.example.json for format).

The only option is "verbose" (boolean) which toggles extra info being logged to the console.
