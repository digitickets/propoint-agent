:: Change directory to the path of this batch file
cd %~dp0

:: Restart service running in pm2
call node_modules\.bin\pm2 restart all
