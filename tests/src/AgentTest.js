require('../bootstrap');

const Agent = require('../../src/Agent');
const https = require('https');

const HttpServer = https.Server;
const IoServer = require('socket.io');
const packageJson = require('../../package.json');
const path = require('path');
const socketIoClient = require('socket.io-client');
const TcpProxy = require('../../src/TcpProxy');

describe('Agent', () => {
    let agent;

    const createClient = () => socketIoClient.connect('https://localhost.dtapps.co.uk:8443', {
        transports: ['websocket'],
        rejectUnauthorized: false
    });

    afterEach(() => {
        if (agent) {
            agent.stop();
        }
    });

    it('Should respond to /status request', (done) => {
        agent = new Agent();
        agent.start();

        https.get(
            'https://localhost.dtapps.co.uk:8443/status',
            (res) => {
                expect(res.statusCode).toBe(200);
                done();
            }
        );
    });

    it('Should construct with default values', () => {
        agent = new Agent();
        expect(agent instanceof Agent).toBeTruthy();

        expect(agent.startedAt.getUTCFullYear()).toBe((new Date()).getUTCFullYear());

        expect(agent.defaultCertPath).toBe(path.resolve(__dirname, '..', '..', 'ssl'));
        // Should use default cert with no config.
        expect(agent.sslCertPath).toBe(agent.defaultCertPath + '/localhost.dtapps.co.uk.crt');
        expect(agent.sslKeyPath).toBe(agent.defaultCertPath + '/localhost.dtapps.co.uk.key');

        expect(agent.restartHour).toBe(2);
        expect(agent.verbose).toBeTruthy();

        expect(agent.tcpProxy instanceof TcpProxy).toBeTruthy();
    });

    it('Should use settings from config', () => {
        agent = new Agent({
            maxUptime: 60,
            port: 7000,
            restartHour: 20,
            verbose: false
        });

        expect(agent.maxUptime).toBe(60);
        expect(agent.port).toBe(7000);
        expect(agent.restartHour).toBe(20);
        expect(agent.verbose).toBeFalsy();
    });

    it('Should use sslCertPath from config', () => {
        try {
            new Agent({
                sslCertPath: 'fake.crt'
            });
        } catch (e) {
            expect(e.toString()).toContain(
                'Error: Failed to read SSL cert: Error: ENOENT: no such file or directory'
            );
        }
    });

    it('Should use sslKeyPath from config', () => {
        try {
            new Agent({
                sslKeyPath: 'fake.key'
            });
        } catch (e) {
            expect(e.toString()).toContain(
                'Error: Failed to read SSL key: Error: ENOENT: no such file or directory'
            );
        }
    });

    it('Should fail with empty sslCertPath', () => {
        try {
            new Agent({
                sslCertPath: null
            });
            fail();
        } catch (e) {
            expect(e.toString()).toBe('Error: sslCertPath and sslKeyPath must be set in the config.');
        }
    });

    it('Should fail with empty sslKeyPath', () => {
        try {
            new Agent({
                sslKeyPath: null
            });
            fail();
        } catch (e) {
            expect(e.toString()).toBe('Error: sslCertPath and sslKeyPath must be set in the config.');
        }
    });

    it('Should get uptime', (done) => {
        agent = new Agent();
        setTimeout(() => {
            expect(agent.getUptime()).toBeGreaterThan(0);
            done();
        }, 10);
    });

    it('Should start restart interval on server start', () => {
        agent = new Agent();
        agent.startRestartTimer = jasmine.createSpy();
        expect(agent.startRestartTimer.calls.count()).toBe(0);
        agent.start();
        expect(agent.startRestartTimer.calls.count()).toBe(1);
    });

    it('Should check for restart at interval', (done) => {
        agent = new Agent();
        agent.isRestartRequired = jasmine.createSpy();
        agent.restartCheckIntervalMs = 50;
        agent.start();
        expect(agent.isRestartRequired.calls.count()).toBe(0);
        setTimeout(() => expect(agent.isRestartRequired.calls.count()).toBeGreaterThanOrEqual(0), 40);
        setTimeout(() => expect(agent.isRestartRequired.calls.count()).toBeGreaterThanOrEqual(1), 100);
        setTimeout(() => expect(agent.isRestartRequired.calls.count()).toBeGreaterThanOrEqual(1), 150);
        setTimeout(() => {
            expect(agent.isRestartRequired.calls.count()).toBeGreaterThanOrEqual(2);
            done();
        }, 250);
    });

    it('Should call restart in interval if necessary', (done) => {
        agent = new Agent();
        agent.isRestartRequired = jasmine.createSpy().and.callFake(() => true);
        agent.restart = jasmine.createSpy();
        agent.restartCheckIntervalMs = 50;
        agent.start();
        setTimeout(() => {
            expect(agent.isRestartRequired.calls.count()).toBeGreaterThanOrEqual(1);
            expect(agent.restart.calls.count()).toBeGreaterThanOrEqual(1);
            done();
        }, 250);
    });

    it('Should not restart if restartHour is false', () => {
        agent = new Agent({ restartHour: false });
        expect(agent.isRestartRequired()).toBe('Auto restart disabled');
    });

    it('Should not restart if uptime too small', (done) => {
        agent = new Agent();
        setTimeout(() => {
            expect(agent.getUptime()).toBeGreaterThan(0);
            expect(agent.getUptime()).toBeLessThan(agent.maxUptime);
            expect(agent.isRestartRequired()).toBe('Uptime too low');
            done();
        }, 100);
    });

    it('Should restart if correct hour', () => {
        const currentHour = (new Date()).getHours();
        agent = new Agent({ restartHour: currentHour }); // Set restart hour to current hour.
        agent.maxUptime = 0;
        expect(agent.lastDataAt).toBeNull();
        expect(agent.isRestartRequired() === true).toBeTruthy();
    });

    it('Should not restart if incorrect hour', () => {
        const currentHour = (new Date()).getHours();
        agent = new Agent({ restartHour: (currentHour + 1 % 24) }); // Set the restart hour to the next hour.
        agent.maxUptime = 0;
        expect(agent.lastDataAt).toBeNull();
        expect(agent.isRestartRequired()).toBe('Wrong hour');
    });

    it('Should not restart if data sent recently', () => {
        const currentHour = (new Date()).getHours();
        agent = new Agent({ restartHour: currentHour }); // Set restart hour to current hour.
        agent.maxUptime = 0;
        agent.lastDataAt = new Date(new Date().getTime() - 180000); // 3 minutes ago
        expect(agent.isRestartRequired()).toBe('Data sent recently');
    });

    it('Should restart if last data was not recently', () => {
        const currentHour = (new Date()).getHours();
        agent = new Agent({ restartHour: currentHour }); // Set restart hour to current hour.
        agent.maxUptime = 0;
        agent.lastDataAt = new Date(new Date().getTime() - 660000); // 7 minutes ago
        expect(agent.isRestartRequired() === true).toBeTruthy();
    });

    it('Should exit from restart function', () => {
        const originalExit = global.process.exit;
        global.process.exit = jasmine.createSpy();

        agent = new Agent();
        agent.restart();

        expect(global.process.exit.calls.count()).toBe(1);

        global.process.exit = originalExit;
    });

    it('Should start and restart http and io server', () => {
        agent = new Agent();
        agent.start();
        expect(agent.httpServer instanceof HttpServer).toBeTruthy();
        expect(agent.io instanceof IoServer).toBeTruthy();

        agent.stop();
        expect(agent.httpServer).toBeNull();
        expect(agent.io).toBeNull();

        agent.start();
        expect(agent.httpServer instanceof HttpServer).toBeTruthy();
        expect(agent.io instanceof IoServer).toBeTruthy();
    });

    it('Should accept WebSocket connections', (done) => {
        agent = new Agent();
        agent.start();

        const socket = createClient();

        socket.on('error', (err) => {
            console.error(err);
            fail();
        });

        socket.on('connect', () => {
            expect(socket.connected).toBeTruthy();
            done();
        });
    });

    it('Should send server info to sockets on demand', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        socket.emit('getServerInfo', {}, (data) => {
            expect(data.agentVersion).toBeTruthy();
            expect(data.agentVersion).toBe(packageJson.version);

            expect(data.nodeVersion.length).toBeGreaterThan(0);
            expect(data.nodeVersion).toBe(process.version.substr(1));

            expect(data.agentDirectory).toBe(path.resolve(__dirname, '..', '..'));

            expect(data.agentStartedAt).toBeTruthy();
            expect(new Date(data.agentStartedAt).getUTCFullYear()).toBe(new Date().getUTCFullYear());

            done();
        });
    });

    it('Should send additional server info', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        agent.setAdditionalServerInfo({
            color: 'sparkly red'
        });

        socket.emit('getServerInfo', {}, (data1) => {
            expect(data1.color).toBe('sparkly red');

            agent.setAdditionalServerInfo({});

            socket.emit('getServerInfo', {}, (data2) => {
                expect(data2.color).toBe(undefined);
                done();
            });
        });
    });


    it('Should still send server info if getting device info fails', (done) => {
        agent = new Agent();
        agent.start();

        agent.deviceInfo = {
            getDeviceInfo() {
                return Promise.reject();
            }
        };

        const socket = createClient();

        socket.emit('getServerInfo', {}, (data) => {
            expect(data.agentVersion).toBeTruthy();
            expect(data.agentVersion).toBe(packageJson.version);

            expect(data.hasOwnProperty('machineIdentifier')).toBe(false);

            expect(data.agentDirectory).toBe(path.resolve(__dirname, '..', '..'));

            expect(data.agentStartedAt).toBeTruthy();
            expect(new Date(data.agentStartedAt).getUTCFullYear()).toBe(new Date().getUTCFullYear());

            done();
        });
    });

    it('Should allow disabled restart timer', () => {
        agent = new Agent({
            restartHour: null
        });
        expect(agent.restartCheckInterval).toBe(null);
    });

    it('Should allow client to set restartHour', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        expect(agent.restartHour).not.toBe(18);

        socket.emit(
            'configure',
            { restartHour: 18 },
            () => {
                expect(agent.restartHour).toBe(18);
                done();
            }
        );
    });

    it('Should allow client to clear restartHour', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        expect(agent.restartCheckInterval).not.toBe(null);
        expect(agent.restartHour).not.toBe(null);

        socket.emit(
            'configure',
            { restartHour: null },
            () => {
                expect(agent.restartCheckInterval).toBe(null);
                expect(agent.restartHour).toBe(null);
                done();
            }
        );
    });

    it('Should allow client to enable restartHour', (done) => {
        agent = new Agent({
            restartHour: null
        });
        agent.start();
        const socket = createClient();

        expect(agent.restartCheckInterval).toBe(null);
        expect(agent.restartHour).toBe(null);

        socket.emit(
            'configure',
            { restartHour: 1 },
            () => {
                expect(agent.restartCheckInterval).not.toBe(null);
                expect(agent.restartHour).toBe(1);
                done();
            }
        );
    });

    it('Should allow restartHour to be set to midnight', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        expect(agent.restartHour).not.toBe(0);

        socket.emit(
            'configure',
            { restartHour: 0 },
            () => {
                expect(agent.restartHour).toBe(0);
                done();
            }
        );
    });

    it('Should not allow restartHour to be set to invalid value', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        expect(agent.restartHour).toBe(2);

        socket.emit(
            'configure',
            { restartHour: 'dinosaur' },
            () => {
                expect(agent.restartHour).toBe(2);
                done();
            }
        );
    });

    it('Should do nothing when configuring with unsupported options', (done) => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        expect(agent.restartHour).toBe(2);

        socket.emit(
            'configure',
            { someVal: 18 },
            () => {
                expect(agent.restartHour).toBe(2);
                done();
            }
        );
    });

    it('Should allow configure without callback', () => {
        agent = new Agent();
        agent.start();
        const socket = createClient();

        expect(agent.restartHour).not.toBe(18);

        socket.emit(
            'configure',
            { hello: 'world' }
        );
    });
});
