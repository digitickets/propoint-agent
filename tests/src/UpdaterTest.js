require('../bootstrap');

const Updater = require('../../src/Updater');
const fs = require('fs');
const path = require('path');
const tempFileFactory = require('../../src/tempFileFactory');

describe('Updater', () => {
    it('Should construct', () => {
        const updater = new Updater();
        expect(updater instanceof Updater).toBeTruthy();
        expect(updater.username).toBe('');
        expect(updater.isWindows).toBeUndefined();
    });

    it('Should detect Windows', () => {
        const updater = new Updater({}, 'win32');
        expect(updater.isWindows).toBeTruthy();
    });

    it('Should not confuse Darwin with Windows', () => {
        const updater = new Updater({}, 'darwin');
        expect(updater.isWindows).toBeFalsy();
    });

    it('Should set properties from config', () => {
        const updater = new Updater({ username: 'user' });
        expect(updater.username).toBe('user');
    });

    it('Should ignore nonexistent options', () => {
        const updater = new Updater({ password: 'pass', someOpt: 'someVal' });
        expect(updater.password).toBe('pass');
    });

    it('Should set full path for zip options', () => {
        const updater = new Updater({ unzipDestination: './', zipPath: './zip-name.zip' });
        expect(updater.unzipDestination).toBe(path.resolve(__dirname, '..', '..', 'src') + '/.././');
        expect(updater.zipPath).toBe(path.resolve(__dirname, '..', '..', 'src') + '/.././zip-name.zip');
    });

    it('Should set zipUrlProtocol from options', () => {
        const updater = new Updater({ zipUrlProtocol: 'ftp' });
        expect(updater.zipUrlProtocol).toBe('ftp');
    });
});

describe('Updater.download', () => {
    /**
     * @type {{path: string, remove: fileCallback}}
     */
    let tempDownloadFile;

    beforeEach(() => {
        tempDownloadFile = tempFileFactory.makeFile('dt-agent-updater-test');
    });

    afterEach(() => {
        if (tempDownloadFile) {
            tempDownloadFile.remove();
        }
    });

    it('Should download file', (done) => {
        const updater = new Updater();

        // URL contains a file that says "Hello world"
        updater.download('http', 'downloads.dtapps.co.uk', '/test.txt', tempDownloadFile.path)
            .then(
                (downloadedPath) => {
                    expect(downloadedPath).toEqual(tempDownloadFile.path);
                    expect(fs.existsSync(downloadedPath)).toBeTruthy();
                    expect(fs.readFileSync(downloadedPath).toString()).toEqual('Hello world\n');
                    done();
                }
            )
            .catch(done.fail);
    });

    it('Should download file over https', (done) => {
        const updater = new Updater();
        updater.download('https', 'downloads.dtapps.co.uk', '/test.txt', tempDownloadFile.path)
            .then((downloadedPath) => {
                expect(downloadedPath).toEqual(tempDownloadFile.path);
                expect(fs.existsSync(downloadedPath)).toBeTruthy();
                expect(fs.readFileSync(downloadedPath).toString().indexOf('Hello world')).toBe(0);
                done();
            })
            .catch(done.fail);
    });

    it('Should download file over http', (done) => {
        const updater = new Updater();
        updater.download('http', 'httpforever.com', '/', tempDownloadFile.path)
            .then((downloadedPath) => {
                expect(downloadedPath).toEqual(tempDownloadFile.path);
                expect(fs.existsSync(downloadedPath)).toBeTruthy();
                expect(fs.readFileSync(downloadedPath).toString()).toBeTruthy();
                done();
            })
            .catch(done.fail);
    });

    it('Should reject download promise on 404', (done) => {
        const updater = new Updater();
        updater.download('http', 'postman-echo.com', '/status/404', tempDownloadFile.path)
            .then(done.fail)
            .catch((error) => {
                expect(error).toEqual('Download Failed. HTTP Status Code: 404');
                done();
            });
    });

    it('Should reject download promise on other HTTP error', (done) => {
        const updater = new Updater();
        updater.download('http', 'postman-echo.com', '/status/502', tempDownloadFile.path)
            .then(done.fail)
            .catch((error) => {
                expect(error).toEqual('Download Failed. HTTP Status Code: 502');
                done();
            });
    });

    it('Should delete file on HTTP error', (done) => {
        const updater = new Updater();

        fs.writeFileSync(tempDownloadFile.path, 'Test contents');
        expect(fs.existsSync(tempDownloadFile.path)).toBeTruthy();

        updater.download('http', 'postman-echo.com', '/status/418', tempDownloadFile.path)
            .then(done.fail)
            .catch(() => {
                expect(fs.existsSync(tempDownloadFile.path)).toBeFalsy();
                done();
            });
    });

    it('Should reject download promise on other error', (done) => {
        const updater = new Updater();
        updater.download('http', 'fake-domain.digitickets', '/path', tempDownloadFile.path)
            .then(done.fail)
            .catch((error) => {
                expect(error.indexOf('Download Failed. Request Error: ')).toBe(0);
                expect(error.indexOf('ENOTFOUND')).not.toBe(-1);
                done();
            });
    });

    it('Should delete file on other error', (done) => {
        const updater = new Updater();

        fs.writeFileSync(tempDownloadFile.path, 'Test contents 2');
        expect(fs.existsSync(tempDownloadFile.path)).toBeTruthy();

        updater.download('http', 'fake-domain.digitickets', '/path', tempDownloadFile.path)
            .then(done.fail)
            .catch(() => {
                expect(fs.existsSync(tempDownloadFile.path)).toBeFalsy();
                done();
            });
    });
});

describe('Updater.unzip', () => {
    /**
     * @type {{path: string, remove: fileCallback}}
     */
    let destDir;

    const srcZipPath = path.resolve(__dirname, '..', 'fixtures') + '/test.zip';

    beforeEach(() => {
        destDir = tempFileFactory.makeDirectory('dt-agent-updater-test');
    });

    afterEach(() => {
        if (destDir) {
            destDir.remove();
        }
    });

    it('Should unzip file', (done) => {
        const updater = new Updater();

        updater.unzip(srcZipPath, destDir.path)
            .then((unzippedTo) => {
                expect(unzippedTo).toEqual(destDir.path);

                expect(fs.readdirSync(destDir.path)).toEqual(['test.txt']);
                expect(fs.existsSync(destDir.path + '/test.txt')).toBeTruthy();
                expect(fs.readFileSync(destDir.path + '/test.txt').toString()).toEqual('Hello world\n');

                done();
            })
            .catch(done.fail);
    });

    it('Should reject if unzip fails', (done) => {
        const updater = new Updater();

        const zipPath = path.resolve(__dirname, '..', 'fixtures') + '/test.txt'; // Not a .zip file.

        updater.unzip(zipPath, destDir.path)
            .then(done.fail)
            .catch((error) => {
                expect(error.length).toBeGreaterThan(0);
                done();
            });
    });
});

describe('Updater.install', () => {
    const mockSpawn = () => ({
        on() {
        },
        stdout: {
            on() {
            }
        },
        stderr: {
            on() {
            }
        }
    });

    it('Should run npm install cmd', () => {
        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.callFake(mockSpawn);

        updater.install();

        expect(updater.spawn.calls.count()).toBe(1);
        expect(updater.spawn.calls.mostRecent().args[0]).toEqual('npm');
        expect(updater.spawn.calls.mostRecent().args[1][0]).toEqual('install');
    });

    it('Should run npm.cmd on Windows', () => {
        const updater = new Updater({}, 'win32');
        updater.spawn = jasmine.createSpy('spawn').and.callFake(mockSpawn);

        updater.install();

        expect(updater.spawn.calls.count()).toBe(1);
        expect(updater.spawn.calls.mostRecent().args[0]).toEqual('npm.cmd');
        expect(updater.spawn.calls.mostRecent().args[1][0]).toEqual('install');
    });

    it('Should write errors to console', () => {
        const originalConsoleLog = global.console.log;
        global.console.log = jasmine.createSpy();

        const spawn = mockSpawn();
        const errorHandlers = [];
        spawn.on = (event, handler) => {
            if (event === 'error') {
                errorHandlers.push(handler);
            }
        };

        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.returnValue(spawn);

        updater.install();

        errorHandlers[0]('Test error');
        expect(global.console.log.calls.count()).toBe(1);
        expect(global.console.log.calls.mostRecent().args).toEqual(
            [
                'Install Error:',
                'Test error',
            ]
        );

        global.console.log = originalConsoleLog;
    });

    it('Should write stdout to console', () => {
        const originalConsoleLog = global.console.log;
        global.console.log = jasmine.createSpy();

        const spawn = mockSpawn();
        const dataHandlers = [];
        spawn.stdout.on = (event, handler) => {
            if (event === 'data') {
                dataHandlers.push(handler);
            }
        };

        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.returnValue(spawn);

        updater.install();

        dataHandlers[0]('Test data');
        expect(global.console.log.calls.count()).toBe(1);
        expect(global.console.log.calls.mostRecent().args).toEqual(
            [
                'Install Output:',
                'Test data',
            ]
        );

        global.console.log = originalConsoleLog;
    });

    it('Should write stderr to console', () => {
        const originalConsoleLog = global.console.log;
        global.console.log = jasmine.createSpy();

        const spawn = mockSpawn();
        const errorHandlers = [];
        spawn.stderr.on = (event, handler) => {
            if (event === 'data') {
                errorHandlers.push(handler);
            }
        };

        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.returnValue(spawn);

        updater.install();

        errorHandlers[0]('Test error');
        expect(global.console.log.calls.count()).toBe(1);
        expect(global.console.log.calls.mostRecent().args).toEqual(
            [
                'Install Error:',
                'Test error',
            ]
        );

        global.console.log = originalConsoleLog;
    });

    it('Should resolve on exit code 0 as string', (done) => {
        const spawn = mockSpawn();
        const exitHandlers = [];
        spawn.on = (event, handler) => {
            if (event === 'exit') {
                exitHandlers.push(handler);
            }
        };

        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.returnValue(spawn);

        updater.install()
            .then(done)
            .catch(done.fail);

        exitHandlers[0]('0');
    });

    it('Should resolve on exit code 0 as number', (done) => {
        const spawn = mockSpawn();
        const exitHandlers = [];
        spawn.on = (event, handler) => {
            if (event === 'exit') {
                exitHandlers.push(handler);
            }
        };

        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.returnValue(spawn);

        updater.install()
            .then(done)
            .catch(done.fail);

        exitHandlers[0](0);
    });

    it('Should reject on non-zero exit code', (done) => {
        const spawn = mockSpawn();
        const exitHandlers = [];
        spawn.on = (event, handler) => {
            if (event === 'exit') {
                exitHandlers.push(handler);
            }
        };

        const updater = new Updater();
        updater.spawn = jasmine.createSpy('spawn').and.returnValue(spawn);

        updater.install()
            .then(done.fail)
            .catch(done);

        exitHandlers[0](1);
    });
});

describe('Updater.downloadAndUnzip', () => {
    it('Should reject if download rejects', (done) => {
        const updater = new Updater();
        updater.download = () => Promise.reject();

        updater.downloadAndUnzip()
            .then(done.fail)
            .catch(done);
    });

    it('Should reject if unzip rejects', (done) => {
        const updater = new Updater();
        updater.download = () => Promise.resolve(__dirname + '/../fixtures/test.zip');
        updater.unzip = () => Promise.reject();

        updater.downloadAndUnzip()
            .then(done.fail)
            .catch(done);
    });

    it('Should resolve if download and unzip resolve', (done) => {
        const updater = new Updater();
        updater.download = () => Promise.resolve(__dirname + '/../fixtures/test.zip');
        updater.unzip = () => Promise.resolve();

        updater.downloadAndUnzip()
            .then(done)
            .catch(done.fail);
    });
});

describe('Updater.attemptUpdateThenInstall', () => {
    it('Should continue to install if update succeeds', (done) => {
        const updater = new Updater();
        updater.downloadAndUnzip = jasmine.createSpy().and.callFake(() => Promise.resolve());
        updater.install = jasmine.createSpy().and.callFake(() => Promise.resolve());

        updater.attemptUpdateThenInstall()
            .then(() => {
                expect(updater.install.calls.count()).toBe(1);
                done();
            })
            .catch(done.fail);
    });

    it('Should continue to install if update fails', (done) => {
        const updater = new Updater();
        updater.downloadAndUnzip = jasmine.createSpy().and.callFake(() => Promise.reject());
        updater.install = jasmine.createSpy().and.callFake(() => Promise.resolve());

        updater.attemptUpdateThenInstall()
            .then(() => {
                expect(updater.install.calls.count()).toBe(1);
                done();
            })
            .catch(done.fail);
    });

    it('Should reject if install rejects', (done) => {
        const updater = new Updater();
        updater.downloadAndUnzip = jasmine.createSpy().and.callFake(() => Promise.resolve());
        updater.install = jasmine.createSpy().and.callFake(() => Promise.reject());

        updater.attemptUpdateThenInstall()
            .then(done.fail)
            .catch(done);
    });
});
