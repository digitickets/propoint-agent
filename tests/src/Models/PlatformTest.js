require('../../bootstrap');

const Platform = require('../../../src/Models/Platform');

describe('Platform', () => {
    describe('getName', () => {
        it('Should return palatform name', () => {
            expect((new Platform('qwertyuiop')).getName()).toBe('qwertyuiop');
        });
    });

    describe('isWindows', () => {
        it('Should return true if Windows', () => {
            expect((new Platform('win32')).isWindows()).toBe(true);
            expect((new Platform('win')).isWindows()).toBe(true);
            expect((new Platform('windows')).isWindows()).toBe(true);
        });

        it('Should return false if not Windows', () => {
            expect((new Platform('darwin')).isWindows()).toBe(false);
            expect((new Platform('linux')).isWindows()).toBe(false);
        });
    });

    describe('isMac', () => {
        it('Should return true if Mac', () => {
            expect((new Platform('darwin')).isMac()).toBe(true);
        });

        it('Should return false if not Mac', () => {
            expect((new Platform('macos')).isMac()).toBe(false); // not valid
            expect((new Platform('mac')).isMac()).toBe(false); // not valid
            expect((new Platform('win32')).isMac()).toBe(false);
            expect((new Platform('win')).isMac()).toBe(false);
            expect((new Platform('windows')).isMac()).toBe(false);
            expect((new Platform('linux')).isMac()).toBe(false);
        });
    });
});
