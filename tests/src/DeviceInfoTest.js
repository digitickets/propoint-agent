require('../bootstrap');

const DeviceInfo = require('../../src/DeviceInfo');

describe('DeviceInfo', () => {
    /**
     * @type {DeviceInfo}
     */
    let deviceInfo;

    beforeEach(() => {
        deviceInfo = new DeviceInfo();
    });

    describe('getDeviceInfo', () => {
        it('Should return device info', (done) => {
            deviceInfo.getDeviceInfo().then((results) => {
                console.warn(results);
                expect(results.machineIdentifier).toBeTruthy();
                expect(results.systemModel.length).toBeGreaterThan(0);
                expect(results.systemModel.indexOf('CPU')).not.toBe(-1);
                expect(results.systemModel.indexOf('RAM')).not.toBe(-1);
                expect(results.computerName.length).toBeGreaterThan(0);

                expect(results.internalIp.length).toBeGreaterThan(0);
                expect(results.internalIp).toMatch(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);

                expect(results.externalIp.length).toBeGreaterThan(0);
                expect(results.externalIp).toMatch(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);
                expect(results.nodeVersion.length).toBeGreaterThan(0);
                done();
            });
        });

        it('Should skip info that fails', (done) => {
            deviceInfo.getMachineIdentifier = () => Promise.reject();
            deviceInfo.getSystemModel = () => Promise.reject();
            deviceInfo.getExternalIp = () => Promise.reject();
            deviceInfo.getInternalIp = () => Promise.reject();

            deviceInfo.si = {
                uuid() {
                    return Promise.reject();
                },
                osInfo() {
                    return Promise.reject();
                }
            };

            deviceInfo.getDeviceInfo().then((results) => {
                console.warn(results);

                expect(results.machineIdentifier).toBeNull();
                expect(results.systemModel).toBeNull();
                expect(results.computerName).toBeNull();
                expect(results.internalIp).toBeNull();
                expect(results.externalIp).toBeNull();
                expect(results.nodeVersion).not.toBeNull();

                done();
            });
        });
    });

    describe('getSystemModel', () => {
        it('Should skip missing info', (done) => {
            deviceInfo.si = {
                system() {
                    return Promise.resolve({});
                },
                cpu() {
                    return Promise.resolve({});
                },
                mem() {
                    return Promise.resolve({});
                }
            };

            deviceInfo.getSystemModel().then((result) => {
                expect(result).toBe('');
                done();
            });
        });

        it('Should create system string', (done) => {
            deviceInfo.si = {
                system() {
                    return Promise.resolve({
                        manufacturer: 'DigiTickets',
                        model: 'ProBox'
                    });
                },
                cpu() {
                    return Promise.resolve({
                        manufacturer: 'Intel(R)',
                        brand: 'Pentium II',
                        speed: '0.233'
                    });
                },
                mem() {
                    return Promise.resolve({
                        total: 33554432
                    });
                }
            };

            deviceInfo.getSystemModel().then((result) => {
                expect(result).toBe('DigiTickets ProBox (Intel(R) Pentium II 0.233GHz CPU) (0.03GB RAM)');
                done();
            });
        });
    });

    describe('getExternalIp', () => {
        it('Should reject on request failure', (done) => {
            deviceInfo.externalIpUrl = 'https://somefakedomainfordigiticketstes.whowouldbuythiscom/ip';
            deviceInfo.getExternalIp()
                .then(() => {
                    done.fail();
                })
                .catch(() => {
                    done();
                });
        });

        it('Should reject on malformed JSON', (done) => {
            // This URL should return a valid response but not JSON.
            deviceInfo.externalIpUrl = 'https://epos.digitickets.co.uk';
            deviceInfo.getExternalIp()
                .then(() => {
                    done.fail();
                })
                .catch(() => {
                    done();
                });
        });

        it('Should reject on JSON without IP', (done) => {
            // This URL should return JSON but not the JSON we want.
            deviceInfo.externalIpUrl = 'https://memberships.digitickets.co.uk/';
            deviceInfo.getExternalIp()
                .then(() => {
                    done.fail();
                })
                .catch(() => {
                    done();
                });
        });

        it('Should resolve with an IP address', (done) => {
            deviceInfo.getExternalIp()
                .then((result) => {
                    expect(result).toMatch(/^(?:(?:^|\.)(?:2(?:5[0-5]|[0-4]\d)|1?\d?\d)){4}$/);
                    done();
                });
        });
    });

    describe('getInternalIp', () => {
        it('Should reject on networkInterfaces failure', (done) => {
            deviceInfo.si = {
                networkInterfaces() {
                    return Promise.reject();
                }
            };

            deviceInfo.getInternalIp()
                .then(() => {
                    done.fail();
                })
                .catch(() => {
                    done();
                });
        });

        it('Should reject if default interface not defined failure', (done) => {
            deviceInfo.si = {
                networkInterfaces() {
                    return Promise.resolve(
                        [
                            {
                                iface: 'some name'
                            },
                        ]
                    );
                },
                networkInterfaceDefault() {
                    return Promise.resolve('some other name');
                }
            };

            deviceInfo.getInternalIp()
                .then(() => {
                    done.fail();
                })
                .catch((err) => {
                    console.warn(err);
                    done();
                });
        });
    });
});
