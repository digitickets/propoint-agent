require('../../bootstrap');

const ConsoleLogger = require('../../../src/Logging/ConsoleLogger');

describe('ConsoleLogger', () => {
    let backupConsoleError;
    let backupConsoleLog;
    let backupConsoleDebug;

    beforeAll(() => {
        backupConsoleError = global.console.error;
        backupConsoleLog = global.console.log;
        backupConsoleDebug = global.console.debug;
    });

    afterAll(() => {
        global.console.error = backupConsoleError;
        global.console.log = backupConsoleLog;
        global.console.debug = backupConsoleDebug;
    });

    describe('error', () => {
        it('Should call console.error', () => {
            global.console.error = jasmine.createSpy();
            const logger = new ConsoleLogger('test');
            logger.error('1', 2, 'three');
            expect(global.console.error).toHaveBeenCalledWith(
                '[test]',
                '1',
                2,
                'three'
            );
        });
    });

    describe('log', () => {
        it('Should call console.log if debug enabled', () => {
            global.console.log = jasmine.createSpy();
            const logger = new ConsoleLogger('test', true);
            logger.log('1', 2, 'three');
            expect(global.console.log).toHaveBeenCalledWith(
                '[test]',
                '1',
                2,
                'three'
            );
        });

        it('Should call console.log if debug disabled', () => {
            global.console.log = jasmine.createSpy();
            const logger = new ConsoleLogger('test', false);
            logger.log('1', 2, 'three');
            expect(global.console.log).toHaveBeenCalledWith(
                '[test]',
                '1',
                2,
                'three'
            );
        });
    });

    describe('debug', () => {
        it('Should call console.debug if debug enabled', () => {
            global.console.debug = jasmine.createSpy();
            const logger = new ConsoleLogger('test', true);
            logger.debug('1', 2, 'three');
            expect(global.console.debug).toHaveBeenCalledWith(
                '[test]',
                '1',
                2,
                'three'
            );
        });

        it('Should not call console.debug if debug disabled', () => {
            global.console.debug = jasmine.createSpy();
            const logger = new ConsoleLogger('test', false);
            logger.debug('1', 2, 'three');
            expect(global.console.debug).not.toHaveBeenCalled();
        });

        it('Should allow enabling debug after construction', () => {
            const logger = new ConsoleLogger('test', false);

            global.console.debug = jasmine.createSpy();
            logger.debug('1', 2, 'three');
            expect(global.console.debug).not.toHaveBeenCalled();

            global.console.debug = jasmine.createSpy();
            logger.setDebugEnabled(true);
            logger.debug('1', 2, 'three');
            expect(global.console.debug).toHaveBeenCalledWith(
                '[test]',
                '1',
                2,
                'three'
            );

            global.console.debug = jasmine.createSpy();
            logger.setDebugEnabled(false);
            logger.debug('1', 2, 'three');
            expect(global.console.debug).not.toHaveBeenCalled();
        });
    });
});
