require('../../bootstrap');

const ConsoleLogger = require('../../../src/Logging/ConsoleLogger');
const logFactory = require('../../../src/Logging/logFactory');

describe('logFactory', () => {
    it('Should create logger', () => {
        const result = logFactory('something', true);
        expect(result instanceof ConsoleLogger).toBe(true);
    });
});
