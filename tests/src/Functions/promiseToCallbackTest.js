require('../../bootstrap');

const promiseToCallback = require('../../../src/Functions/promiseToCallback');

describe('promiseToCallback', () => {
    it('Should call callback with error on rejection', (done) => {
        let cb = (data) => {
            expect(data).toEqual({ error: 'error message' });
            done();
        };

        promiseToCallback(
            Promise.reject(new Error('error message')),
            cb
        );
    });

    it('Should call callback with success for promise returning string', (done) => {
        let cb = (data) => {
            expect(data).toEqual({ success: true, result: 'hello world' });
            done();
        };

        promiseToCallback(
            Promise.resolve('hello world'),
            cb
        );
    });

    it('Should call callback with success for promise returning object', (done) => {
        let cb = (data) => {
            expect(data).toEqual({ success: true, text: 'hello world' });
            done();
        };

        promiseToCallback(
            Promise.resolve({ text: 'hello world' }),
            cb
        );
    });

    it('Should call callback with success for promise returning object already containing success', (done) => {
        let cb = (data) => {
            expect(data).toEqual({ success: true, text: 'hello world' });
            done();
        };

        promiseToCallback(
            Promise.resolve({ success: true, text: 'hello world' }),
            cb
        );
    });

    it('Should call callback with success for promise returning object already containing falsy success', (done) => {
        let cb = (data) => {
            expect(data).toEqual({ success: false, text: 'hello world' });
            done();
        };

        promiseToCallback(
            Promise.resolve({ success: false, text: 'hello world' }),
            cb
        );
    });
});
