require('../bootstrap');

const Agent = require('../../src/Agent');
const fs = require('fs');
const rimraf = require('rimraf');
const socketIoClient = require('socket.io-client');
const tempFileFactory = require('../../src/tempFileFactory');
const Filesystem = require('../../src/Filesystem');

describe('FilesystemServer', () => {
    /**
     * @type {Agent}
     */
    let agent;

    let client;

    /**
     * @type {{path: string, remove: Function}}
     */
    let testFile;

    let dataDir;

    beforeEach(() => {
        agent = new Agent();
        agent.start();
        client = socketIoClient.connect('https://localhost.dtapps.co.uk:8443', {
            transports: ['websocket'],
            rejectUnauthorized: false
        });

        testFile = tempFileFactory.makeFile('dt-agent-filesystem-test', '.txt');
        dataDir = Filesystem.getDefaultDataDir(agent.platform);
    });

    afterEach(() => {
        if (agent) {
            agent.stop();
        }
        if (testFile) {
            testFile.remove();
        }
    });

    // The ability to read arbitrary files is disabled until we can add some security.
    // See https://digitickets.atlassian.net/browse/PROP-1475
    // describe('writeFile', () => {
    //     it('Should require path', (done) => {
    //         client.emit(
    //             'writeFile',
    //             {
    //                 data: 'hello'
    //             },
    //             (response) => {
    //                 expect(response.error).toBe('path must be specified.');
    //                 done();
    //             }
    //         );
    //     });
    //
    //     it('Should require data', (done) => {
    //         client.emit(
    //             'writeFile',
    //             {
    //                 path: testFile.path
    //             },
    //             (response) => {
    //                 expect(response.error).toBe('data must be specified (but can be empty).');
    //                 done();
    //             }
    //         );
    //     });
    //
    //     it('Should write file', (done) => {
    //         testFile.remove();
    //
    //         const data = 'Hello world ' + new Date().toISOString();
    //         client.emit(
    //             'writeFile',
    //             {
    //                 path: testFile.path,
    //                 data
    //             },
    //             (response) => {
    //                 expect(response.path).toBe(testFile.path);
    //                 expect(response.success).toBeTruthy();
    //                 expect(fs.existsSync(testFile.path)).toBeTruthy();
    //                 expect(fs.readFileSync(testFile.path).toString()).toEqual(data);
    //
    //                 done();
    //             }
    //         );
    //     });
    // });

    describe('writeFileToDataDir', () => {
        it('Should require path', (done) => {
            client.emit(
                'writeFileToDataDir',
                {
                    data: 'hello'
                },
                (response) => {
                    expect(response.error)
                        .toBe('path must be specified.');
                    done();
                }
            );
        });

        it('Should require data', (done) => {
            client.emit(
                'writeFileToDataDir',
                {
                    path: testFile.path
                },
                (response) => {
                    expect(response.error)
                        .toBe('data must be specified (but can be empty).');
                    done();
                }
            );
        });

        it('Should write file', (done) => {
            const data = 'Hello world ' + new Date().toISOString();
            client.emit(
                'writeFileToDataDir',
                {
                    path: 'test-file-to-write.txt',
                    data
                },
                (response) => {
                    const path = agent.filesystemServer.filesystem.dataDir + agent.filesystemServer.filesystem.pathSeparator + 'test-file-to-write.txt';
                    expect(response.path)
                        .toBe(path);
                    expect(response.success)
                        .toBeTruthy();
                    expect(fs.existsSync(path))
                        .toBeTruthy();
                    expect(fs.readFileSync(path)
                        .toString())
                        .toEqual(data);

                    done();
                }
            );
        });

        it('Should prepend data dir to path and error', (done) => {
            client.emit(
                'writeFileToDataDir',
                {
                    path: '/etc/passwd',
                    data: ''
                },
                (response) => {
                    expect(response.error)
                        .toBe(`ENOENT: no such file or directory, open '${dataDir}/etc/passwd'`);
                    done();
                }
            );
        });

        it('Should prevent access outside data dir', (done) => {
            client.emit(
                'writeFileToDataDir',
                {
                    path: '../../../etc/passwd',
                    data: ''
                },
                (response) => {
                    expect(response.error)
                        .toBe('Invalid path.');
                    done();
                }
            );
        });
    });

    // The ability to read arbitrary files is disabled until we can add some security.
    // See https://digitickets.atlassian.net/browse/PROP-1475
    // describe('appendFile', () => {
    //     it('Should require path', (done) => {
    //         client.emit(
    //             'appendFile',
    //             {
    //                 data: 'hello'
    //             },
    //             (response) => {
    //                 expect(response.error).toBe('path must be specified.');
    //                 done();
    //             }
    //         );
    //     });
    //
    //     it('Should require data', (done) => {
    //         client.emit(
    //             'appendFile',
    //             {
    //                 path: testFile.path
    //             },
    //             (response) => {
    //                 expect(response.error).toBe('data must be specified (but can be empty).');
    //                 done();
    //             }
    //         );
    //     });
    //
    //     it('Should handle error', (done) => {
    //         client.emit(
    //             'appendFile',
    //             {
    //                 path: '/fake/path',
    //                 data: 'hello'
    //             },
    //             (response) => {
    //                 expect(response.error).toBe('ENOENT: no such file or directory, open \'/fake/path\'');
    //                 done();
    //             }
    //         );
    //     });
    //
    //     it('Should append to file', (done) => {
    //         testFile.remove();
    //
    //         client.emit(
    //             'appendFile',
    //             {
    //                 path: testFile.path,
    //                 data: 'hello'
    //             },
    //             (response1) => {
    //                 expect(response1.path).toBe(testFile.path);
    //                 expect(response1.success).toBeTruthy();
    //                 expect(fs.existsSync(testFile.path)).toBeTruthy();
    //                 expect(fs.readFileSync(testFile.path).toString()).toEqual('hello');
    //
    //                 client.emit(
    //                     'appendFile',
    //                     {
    //                         path: testFile.path,
    //                         data: ' world'
    //                     },
    //                     (response2) => {
    //                         expect(response2.path).toBe(testFile.path);
    //                         expect(response2.success).toBeTruthy();
    //                         expect(fs.readFileSync(testFile.path).toString()).toEqual('hello world');
    //
    //                         done();
    //                     }
    //                 );
    //             }
    //         );
    //     });
    // });

    describe('appendFileToDataDir', () => {
        it('Should require path', (done) => {
            client.emit(
                'appendFileToDataDir',
                {
                    data: 'hello'
                },
                (response) => {
                    console.debug('response', response);
                    expect(response.error)
                        .toBe('path must be specified.');
                    done();
                }
            );
        });

        it('Should require data', (done) => {
            client.emit(
                'appendFileToDataDir',
                {
                    path: testFile.path
                },
                (response) => {
                    expect(response.error)
                        .toBe('data must be specified (but can be empty).');
                    done();
                }
            );
        });

        it('Should append file', (done) => {
            const filename = 'test-file-for-append.txt';
            const expectedPath = agent.filesystemServer.filesystem.dataDir
                + agent.filesystemServer.filesystem.pathSeparator
                + filename;
            rimraf.sync(expectedPath);

            const data1 = 'Hello world ' + new Date().toISOString();
            const data2 = 'Hello world ' + new Date().toISOString();

            client.emit(
                'appendFileToDataDir',
                {
                    path: filename,
                    data: data1
                },
                (response1) => {
                    expect(response1.path)
                        .toBe(expectedPath);
                    expect(response1.success)
                        .toBe(true);
                    expect(fs.readFileSync(expectedPath)
                        .toString())
                        .toEqual(data1);

                    client.emit(
                        'appendFileToDataDir',
                        {
                            path: filename,
                            data: data2
                        },
                        (response2) => {
                            expect(response2.path)
                                .toBe(expectedPath);
                            expect(response2.success)
                                .toBe(true);
                            expect(fs.readFileSync(expectedPath)
                                .toString())
                                .toEqual(data1 + data2);
                            // Note that no line separators are added. It's up the client to do that.

                            done();
                        }
                    );
                }
            );
        });

        it('Should prepend data dir to path and error', (done) => {
            client.emit(
                'appendFileToDataDir',
                {
                    path: '/etc/passwd',
                    data: ''
                },
                (response) => {
                    expect(response.error)
                        .toBe(`ENOENT: no such file or directory, open '${dataDir}/etc/passwd'`);
                    done();
                }
            );
        });

        it('Should prevent access outside data dir', (done) => {
            client.emit(
                'appendFileToDataDir',
                {
                    path: '../../../etc/passwd',
                    data: ''
                },
                (response) => {
                    expect(response.error)
                        .toBe('Invalid path.');
                    done();
                }
            );
        });
    });

    // The ability to read arbitrary files is disabled until we can add some security.
    // See https://digitickets.atlassian.net/browse/PROP-1475
    // describe('readFile', () => {
    //     it('Should require path', (done) => {
    //         client.emit(
    //             'readFile',
    //             {},
    //             (response) => {
    //                 expect(response.error).toBe('path must be specified.');
    //                 done();
    //             }
    //         );
    //     });
    //
    //     it('Should read file', (done) => {
    //         const data = 'Hello world ' + new Date().toISOString();
    //         fs.writeFileSync(testFile.path, data);
    //         expect(fs.existsSync(testFile.path)).toBeTruthy();
    //
    //         client.emit(
    //             'readFile',
    //             {
    //                 path: testFile.path
    //             },
    //             (response) => {
    //                 expect(response.success).toBeTruthy();
    //                 expect(response.data.length).toBeGreaterThan(0);
    //                 expect(response.data).toBe(data);
    //
    //                 done();
    //             }
    //         );
    //     });
    // });

    describe('readFileFromDataDir', () => {
        it('Should require path', (done) => {
            client.emit(
                'readFileFromDataDir',
                {},
                (response) => {
                    expect(response.error)
                        .toBe('path must be specified.');
                    done();
                }
            );
        });

        it('Should read file', (done) => {
            const path = agent.filesystemServer.filesystem.dataDir + agent.filesystemServer.filesystem.pathSeparator + 'test-file-to-read.txt';
            rimraf.sync(path);
            const data = '' + (new Date()).getTime();

            fs.writeFileSync(path, data);
            expect(fs.existsSync(path))
                .toBeTruthy();

            client.emit(
                'readFileFromDataDir',
                {
                    path: 'test-file-to-read.txt'
                },
                (response) => {
                    expect(response.path)
                        .toBe(path);
                    expect(response.success)
                        .toBe(true);
                    expect(response.data.length)
                        .toBeGreaterThan(0);
                    expect(response.data)
                        .toBe(data);

                    done();
                }
            );
        });

        it('Should support relative path to file', (done) => {
            const data = (new Date()).toISOString();
            fs.writeFileSync(dataDir + '/test-file-to-write.txt', data);

            client.emit(
                'readFileFromDataDir',
                {
                    path: 'test-file-to-write.txt'
                },
                (response) => {
                    expect(response.data).toBe(data);
                    done();
                }
            );
        });

        it('Should support absolute path to file', (done) => {
            const data = (new Date()).toISOString();
            fs.writeFileSync(dataDir + '/test-file-to-write.txt', data);

            client.emit(
                'readFileFromDataDir',
                {
                    path: dataDir + '/test-file-to-write.txt'
                },
                (response) => {
                    expect(response.data).toBe(data);
                    done();
                }
            );
        });


        it('Should prepend data dir to path and error', (done) => {
            client.emit(
                'readFileFromDataDir',
                {
                    path: '/etc/passwd'
                },
                (response) => {
                    expect(response.error)
                        .toBe(`ENOENT: no such file or directory, open '${dataDir}/etc/passwd'`);
                    done();
                }
            );
        });

        it('Should prevent access outside data dir', (done) => {
            client.emit(
                'readFileFromDataDir',
                {
                    path: '../../../etc/passwd'
                },
                (response) => {
                    expect(response.error)
                        .toBe('Invalid path.');
                    done();
                }
            );
        });
    });
});
