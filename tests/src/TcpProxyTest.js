require('../bootstrap');

const socketIoClient = require('socket.io-client');

const Agent = require('../../src/Agent');
const TcpServer = require('../fixtures/tcp/TcpServer');

describe('TcpProxy', () => {
    let agent;
    let client;

    /**
     * @type {TcpServer}
     */
    let tcpServer;

    const tcpServerHost = '127.0.0.1';
    let tcpServerPort = 7800;

    const defaultConnectParams = () => ({
        host: tcpServerHost,
        port: tcpServerPort,
        socketName: 'test'
    });

    beforeEach(() => {
        agent = new Agent();
        agent.start();
        client = socketIoClient.connect('https://localhost.dtapps.co.uk:8443', {
            transports: ['websocket'],
            rejectUnauthorized: false
        });

        // Create a dummy server to connect to.
        tcpServerPort += 1;
        tcpServer = new TcpServer(tcpServerPort, tcpServerHost);
    });

    afterEach(() => {
        if (agent) {
            agent.stop();
            agent = null;
        }
        if (tcpServer) {
            tcpServer.close();
        } else {
            console.warn('TCP server already stopped');
        }
    });

    it('Should require host', (done) => {
        const connectParams = defaultConnectParams();
        delete connectParams.host;

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.error).toBe('host must be specified.');
                done();
            }
        );
    });

    it('Should require non-empty host', (done) => {
        const connectParams = defaultConnectParams();
        connectParams.host = '';

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.error).toBe('host must be specified.');
                done();
            }
        );
    });

    it('Should require port', (done) => {
        const connectParams = defaultConnectParams();
        delete connectParams.port;

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.error).toBe('port must be specified.');
                done();
            }
        );
    });

    it('Should require non-empty port', (done) => {
        const connectParams = defaultConnectParams();
        connectParams.port = '';

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.error).toBe('port must be specified.');
                done();
            }
        );
    });

    it('Should require socketName', (done) => {
        const connectParams = defaultConnectParams();
        delete connectParams.socketName;

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.error).toBe('socketName must be specified.');
                done();
            }
        );
    });

    it('Should require non-empty socketName', (done) => {
        const connectParams = defaultConnectParams();
        connectParams.socketName = '';

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.error).toBe('socketName must be specified.');
                done();
            }
        );
    });

    /**
     * Tests a situation that arose during testing where the server would crash if connectTcpSocket is sent
     * without a callback.
     */
    it('Should not break if connectTcpSocket is sent without callback', (done) => {
        client.emit('connectTcpSocket', defaultConnectParams());
        // Agent would throw an exception if this breaks.
        setTimeout(done, 500);
    });

    it('Should connect to TCP socket', (done) => {
        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                expect(response.socketName).toBe('test');
                expect(response.connected).toBeTruthy();

                expect(response.encoding).toBe('latin1');

                expect(tcpServer.clients.length).toBe(1);

                // Ensure TCP socket is connected to server.
                tcpServer.getConnectionCount()
                    .then((count) => {
                        expect(count).toBe(1);
                        done();
                    });
            }
        );
    });

    it('Should handle exception on TCP socket connect', (done) => {
        const connectParams = defaultConnectParams();
        connectParams.encoding = 'fakefake'; // Use a made up encoding to cause an exception

        client.emit(
            'connectTcpSocket',
            connectParams,
            () => {
                // The callback will never happen.
                fail();
            }
        );

        client.on(
            'tcpSocketError',
            (response) => {
                expect(response.error.indexOf('Unknown encoding: fakefake')).not.toBe(-1);
                done();
            }
        );
    });

    it('Should require unique socket name', (done) => {
        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response1) => {
                expect(response1.connected).toBeTruthy();

                client.emit(
                    'connectTcpSocket',
                    defaultConnectParams(),
                    (response2) => {
                        expect(response2.error).toBe('That socket name is already in use.');
                        done();
                    }
                );
            }
        );
    });

    it('Should receive socket disconnect event', (done) => {
        client.on('tcpSocketClosed', (data) => {
            expect(data.socketName).toBe('test');
            done();
        });

        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                expect(response.connected).toBeTruthy();
                // Close the connection from the server.
                tcpServer.disconnectClients();
            }
        );
    });

    describe('sendTcpData', () => {
        it('Should should not fail without callback provided', (done) => {
            client.emit(
                'sendTcpData',
                {}
            );
            setTimeout(done, 100);
        });
    });

    it('Should require socketName for sendTcpData', (done) => {
        client.emit(
            'sendTcpData',
            {},
            (response) => {
                expect(response.error).toBe('socketName must be specified.');
                done();
            }
        );
    });

    it('Should require socket to exist for sendTcpData', (done) => {
        client.emit(
            'sendTcpData',
            { socketName: 'fake' },
            (response) => {
                expect(response.error).toBe('That socket name does not exist.');
                done();
            }
        );
    });

    it('Should send data to TCP socket', (done) => {
        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                expect(response.connected).toBeTruthy();

                client.emit(
                    'sendTcpData',
                    { socketName: 'test', data: 'Hello world.' },
                    () => {
                        setTimeout(() => {
                            expect(tcpServer.dataReceived.length).toBe(1);
                            expect(tcpServer.dataReceived[0]).toBe('Hello world.');
                            done();
                        }, 100);
                    }
                );
            }
        );
    });

    it('Should update lastDataAt when sending data', (done) => {
        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                expect(response.connected).toBeTruthy();
                const originalLastDataAt = agent.lastDataAt;
                setTimeout(() => {
                    client.emit(
                        'sendTcpData',
                        { socketName: 'test', data: 'Hello world.' },
                        () => {
                            expect(agent.lastDataAt).toBeGreaterThan(originalLastDataAt);
                            done();
                        }
                    );
                }, 2000);
            }
        );
    });

    it('Should receive data from TCP socket', (done) => {
        client.on(
            'tcpSocketData',
            (data) => {
                expect(data.socketName).toBe('test');
                expect(data.data).toBe('Hello world from socket.');
                done();
            }
        );

        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                expect(response.connected).toBeTruthy();
                tcpServer.sendDataToClients('Hello world from socket.');
            }
        );
    });

    it('Should receive UTF8 data from TCP socket with encoding set', (done) => {
        const connectParams = defaultConnectParams();
        connectParams.encoding = 'utf8';

        client.on(
            'tcpSocketData',
            (data) => {
                expect(data.data).toBe('Taco cat 🌮😸');
                done();
            }
        );

        client.emit(
            'connectTcpSocket',
            connectParams,
            (response) => {
                expect(response.connected).toBeTruthy();
                expect(response.encoding).toBe('utf8');
                tcpServer.sendDataToClients('Taco cat 🌮😸');
            }
        );
    });

    it('Should disconnect TCP sockets on WebSocket disconnect', (done) => {
        // Open a TCP socket connection.
        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                console.debug('response', response);
                expect(response.connected).toBeTruthy();

                tcpServer.getConnectionCount()
                    .then((count1) => {
                        expect(count1).toBe(1);

                        // Close the WebSocket connection.
                        client.close();

                        setTimeout(() => {
                            // TCP connection should close.
                            tcpServer.getConnectionCount()
                                .then((count2) => {
                                    expect(count2).toBe(0);
                                    done();
                                });
                        }, 500);
                    });
            }
        );
    });

    it('Should handle already disconnected TCP socket on WebSocket disconnect', (done) => {
        // Open a TCP socket connection.
        client.emit(
            'connectTcpSocket',
            defaultConnectParams(),
            (response) => {
                expect(response.connected).toBeTruthy();

                // Close the connection from the server
                tcpServer.disconnectClients();

                // Close the WebSocket connection.
                client.close();

                setTimeout(() => {
                    done();
                }, 500);
            }
        );
    });

    it('Should handle failure to connect to TCP socket', (done) => {
        const host = 'fake-domain.digitickets';

        const connectParams = defaultConnectParams();
        connectParams.host = host;

        client.emit(
            'connectTcpSocket',
            connectParams,
            () => {
                // The callback will never happen.
                fail();
            }
        );

        client.on(
            'tcpSocketError',
            (response) => {
                expect(response.error.indexOf('Error: getaddrinfo ENOTFOUND ' + host)).toBe(0);
                done();
            }
        );
    });

    describe('disconnectTcpSocket', () => {
        it('Should fail with missing socket name', (done) => {
            client.emit(
                'disconnectTcpSocket',
                {},
                (response) => {
                    expect(response.error).toBe('socketName must be specified.');
                    done();
                }
            );
        });

        it('Should fail with invalid socket name', (done) => {
            client.emit(
                'disconnectTcpSocket',
                {
                    socketName: 'something'
                },
                (response) => {
                    expect(response.error).toBe('That socket name does not exist.');
                    done();
                }
            );
        });

        it('Should disconnect socket', (done) => {
            client.emit(
                'connectTcpSocket',
                defaultConnectParams(),
                (response) => {
                    expect(response.connected).toBeTruthy();
                    expect(tcpServer.clients.length).toBe(1);

                    // Ensure TCP socket is connected to server.
                    tcpServer.getConnectionCount()
                        .then((count) => {
                            expect(count).toBe(1);

                            // Now send the disconnect message.
                            client.emit('disconnectTcpSocket', { socketName: 'test' }, (response2) => {
                                expect(response2).toBe(true);

                                // This number stays as 1 because the TCP server remembers disconnected clients...
                                expect(tcpServer.clients.length).toBe(1);

                                // ... but this check only returns connected clients and should now be 0.
                                tcpServer.getConnectionCount()
                                    .then((count2) => {
                                        expect(count2).toBe(0);

                                        done();
                                    });
                            });
                        });
                }
            );
        });

        it('Should should not fail without callback provided', (done) => {
            client.emit(
                'disconnectTcpSocket',
                {}
            );
            setTimeout(done, 100);
        });
    });
});
