require('../bootstrap');

const Filesystem = require('../../src/Filesystem');
const fs = require('fs');
const Platform = require('../../src/Models/Platform');
const rimraf = require('rimraf');
const tempFileFactory = require('../../src/tempFileFactory');

describe('Filesystem', () => {
    /**
     * @type {Filesystem}
     */
    let filesystem;

    /**
     * @type {{path: string, remove: function}}
     */
    let testFile;

    beforeEach(() => {
        filesystem = new Filesystem(
            new Platform(process.platform),
            {}
        );

        testFile = tempFileFactory.makeFile('dt-agent-filesystem-test', '.txt');
    });

    afterEach(() => {
        if (testFile) {
            testFile.remove();
        }
    });

    describe('constructor', () => {
        it('Should set data dir from config', () => {
            const f = new Filesystem(
                new Platform('win32'),
                {
                    dataDir: 'D:\\cool-stuff'
                }
            );

            expect(f.dataDir).toBe('D:\\cool-stuff');
        });

        it('Should set data dir on Windows', () => {
            const f = new Filesystem(
                new Platform('win32')
            );

            expect(f.dataDir).toBe(Filesystem.defaultWindowsDataDir);
        });

        it('Should set data dir on Mac', () => {
            const f = new Filesystem(
                new Platform('darwin')
            );

            expect(f.dataDir).toBe(Filesystem.defaultUnixDataDir);
        });
    });

    describe('normalizeFilename', () => {
        it('Should fix Windows paths on platform with Unix separator', () => {
            filesystem.pathSeparator = '/';
            const input = '\\hello\\world';
            expect(filesystem.normalizeFilename(input)).toBe('/hello/world');
        });

        it('Should fix Unix paths on platform with Unix separator', () => {
            filesystem.pathSeparator = '/';
            const input = '/hello/world';
            expect(filesystem.normalizeFilename(input)).toBe('/hello/world');
        });

        it('Should fix Windows paths on platform with Windows separator', () => {
            filesystem.pathSeparator = '\\';
            const input = '\\hello\\world';
            expect(filesystem.normalizeFilename(input)).toBe('\\hello\\world');
        });

        it('Should fix Unix paths on platform with Windows separator', () => {
            filesystem.pathSeparator = '\\';
            const input = '/hello/world';
            expect(filesystem.normalizeFilename(input)).toBe('\\hello\\world');
        });
    });

    describe('writeFile', () => {
        it('Should require path', (done) => {
            filesystem.writeFile({})
                .then(done.fail)
                .catch((error) => {
                    expect(error.message).toBe('path must be specified.');
                    done();
                });
        });

        it('Should require non-empty path', (done) => {
            filesystem.writeFile({
                path: ''
            })
                .then(done.fail)
                .catch((error) => {
                    expect(error.message).toBe('path must be specified.');
                    done();
                });
        });

        it('Should require data', (done) => {
            filesystem.writeFile({
                path: '/tmp/some-path'
            })
                .then(done.fail)
                .catch((error) => {
                    expect(error.message).toBe('data must be specified (but can be empty).');
                    done();
                });
        });

        it('Should write file', (done) => {
            testFile.remove();

            const data = 'Hello world ' + new Date().toISOString();
            filesystem.writeFile({
                path: testFile.path,
                data
            }, true)
                .then((response) => {
                    expect(response.path).toBe(testFile.path);
                    expect(response.success).toBeTruthy();
                    expect(fs.existsSync(testFile.path)).toBeTruthy();
                    expect(fs.readFileSync(testFile.path).toString()).toEqual(data);
                    done();
                });
        });

        it('Should overwrite file', (done) => {
            fs.writeFileSync(testFile.path, 'Hello World');
            expect(fs.existsSync(testFile.path)).toBeTruthy();

            filesystem.writeFile({
                path: testFile.path,
                data: 'Hola'
            }, true)
                .then((response) => {
                    expect(response.success).toBeTruthy();
                    expect(fs.existsSync(testFile.path)).toBeTruthy();
                    expect(fs.readFileSync(testFile.path).toString()).toEqual('Hola');
                    done();
                });
        });

        it('Should allow writing empty data', (done) => {
            fs.writeFileSync(testFile.path, 'Hello World');
            expect(fs.existsSync(testFile.path)).toBeTruthy();

            filesystem.writeFile({
                path: testFile.path,
                data: ''
            }, true)
                .then((response) => {
                    expect(response.path).toBe(testFile.path);
                    expect(response.success).toBeTruthy();
                    expect(fs.existsSync(testFile.path)).toBeTruthy();
                    expect(fs.readFileSync(testFile.path).toString()).toEqual('');
                    done();
                });
        });

        it('Should return error for non-writable file', (done) => {
            const path = '/fake-dir-for-tests/test.txt';

            filesystem.writeFile({
                path,
                data: 'Hello world'
            }, true)
                .then(done.fail)
                .catch((error) => {
                    expect(error.message.indexOf('no such file or directory')).not.toBe(-1);
                    expect(fs.existsSync(path)).toBeFalsy();
                    done();
                });
        });
    });

    describe('readFile', () => {
        it('Should require path', (done) => {
            filesystem.readFile({})
                .then(done.fail)
                .catch((error) => {
                    expect(error.message).toBe('path must be specified.');
                    done();
                });
        });

        it('Should require non-empty path', (done) => {
            filesystem.readFile({
                path: ''
            }, true)
                .then(done.fail)
                .catch((error) => {
                    expect(error.message).toBe('path must be specified.');
                    done();
                });
        });

        it('Should return error for non-existent file', (done) => {
            const path = '/fake-dir-for-tests/test.txt';
            expect(fs.existsSync(path)).toBeFalsy();

            filesystem.readFile({
                path
            }, true)
                .then(done.fail)
                .catch((error) => {
                    expect(error.message.length).toBeGreaterThan(0);
                    done();
                });
        });

        it('Should read file', (done) => {
            const data = 'Hello world ' + new Date().toISOString();
            fs.writeFileSync(testFile.path, data);
            expect(fs.existsSync(testFile.path)).toBeTruthy();

            filesystem.readFile({
                path: testFile.path
            }, true)
                .then((response) => {
                    expect(response.success).toBeTruthy();
                    expect(response.data.length).toBeGreaterThan(0);
                    expect(response.data).toBe(data);
                    done();
                });
        });

        it('Should read pound symbol in file', (done) => {
            fs.writeFileSync(testFile.path, '£12.34');
            expect(fs.existsSync(testFile.path)).toBeTruthy();

            filesystem.readFile({
                path: testFile.path
            }, true)
                .then((response) => {
                    expect(response.success).toBeTruthy();
                    expect(response.data).toBe('£12.34');
                    done();
                });
        });

        it('Should read UTF8 file data', (done) => {
            fs.writeFileSync(testFile.path, 'Hello 👻');
            expect(fs.existsSync(testFile.path)).toBeTruthy();

            filesystem.readFile({
                path: testFile.path
            }, true)
                .then((response) => {
                    expect(response.success).toBeTruthy();
                    expect(response.data).toBe('Hello 👻');
                    done();
                });
        });

        it('Should only allow reading inside data dir', (done) => {
            const dataDir = Filesystem.getDefaultDataDir(new Platform(process.platform));

            filesystem.readFile({ path: '/etc/passwd' }, false)
                .catch((err) => {
                    expect(err.message).toContain(`ENOENT: no such file or directory, open '${dataDir}/etc/passwd'`);
                    done();
                });
        });

        it('Should allow reading outside data dir', (done) => {
            filesystem.readFile({ path: '/etc/passwd' }, true)
                .then((response) => {
                    expect(response.data.length).toBeGreaterThan(0);
                    expect(response.success).toBeTruthy();
                    done();
                });
        });
    });

    describe('ensureDataDirExists', () => {
        it('Should create directory if not existing', () => {
            rimraf.sync(filesystem.dataDir);
            expect(fs.existsSync(filesystem.dataDir)).toBe(false);
            filesystem.ensureDataDirExists();
            expect(fs.existsSync(filesystem.dataDir)).toBe(true);
        });

        it('Should do nothing if already existing', () => {
            rimraf.sync(filesystem.dataDir);
            fs.mkdirSync(filesystem.dataDir);
            expect(fs.existsSync(filesystem.dataDir)).toBe(true);
            filesystem.ensureDataDirExists();
            expect(fs.existsSync(filesystem.dataDir)).toBe(true);
        });
    });
});
