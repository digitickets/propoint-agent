// Reduce spam in test output:
const originalConsoleLog = global.console.log;
// Shim for node 6
if (!global.console.debug) {
    global.console.debug = originalConsoleLog;
}
global.console.log = () => {
};

const reporters = require('jasmine-reporters');

const junitReporter = new reporters.JUnitXmlReporter({
    savePath: __dirname + '/../test-reports',
    consolidateAll: true
});
jasmine.getEnv().addReporter(junitReporter);

const JasmineConsoleReporter = require('jasmine-console-reporter');
// See: https://www.npmjs.com/package/jasmine-console-reporter
const consoleReporter = new JasmineConsoleReporter({
    colors: 1,
    cleanStack: 1,
    verbosity: 4,
    listStyle: 'indent',
    activity: 'flip',
    emoji: true,
    beep: true
});
jasmine.getEnv().addReporter(consoleReporter);

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
