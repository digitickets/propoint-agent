const net = require('net');

const TcpServer = function (port, host) {
    this.port = port;
    this.host = host;
    this.clients = [];
    this.dataReceived = [];

    this.server = net.createServer((client) => {
        this.clients.push(client);
        // Store all incoming data from clients.
        client.on('data', data => this.dataReceived.push(data.toString()));

        client.on('close', () => console.warn('TCP Client closed.'));
    });
    this.server.listen(this.port, this.host);

    console.warn(`Starting TCP server on ${this.host}:${this.port}`);
};

TcpServer.prototype = {
    close() {
        console.warn(`Stopping TCP server on ${this.host}:${this.port}`);
        this.server.close(() => {
            this.server.unref();
            console.warn(`Stopped TCP server on ${this.host}:${this.port}`);
        });
    },

    /**
     * @return {Promise<number>}
     */
    getConnectionCount() {
        return new Promise((resolve, reject) => {
            this.server.getConnections((error, count) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(count);
                }
            });
        });
    },

    disconnectClients() {
        for (let i = 0; i < this.clients.length; i++) {
            this.clients[i].end();
            delete this.clients[i];
        }
    },

    sendDataToClients(data) {
        for (let i = 0; i < this.clients.length; i++) {
            this.clients[i].write(data);
        }
    }
};

module.exports = TcpServer;
