:: Change directory to the path of this batch file
cd %~dp0

:: Stop the pm2 service running.
:: After this it will still be loaded in pm2 but not running
call node_modules\.bin\pm2 stop all

:: Shut down pm2
call node_modules\.bin\pm2 kill
