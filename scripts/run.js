const fs = require('fs');
const path = require('path');
const Agent = require('../src/Agent');

// Shim for node 6
if (!global.console.debug) {
    global.console.debug = global.console.log;
}

// Import configuration
let config;

const configFilePath = path.join(__dirname, '../config/config.json');
if (fs.existsSync(configFilePath)) {
    const configJson = fs.readFileSync(configFilePath).toString();
    config = JSON.parse(configJson);
} else {
    config = {};
}

// Run server
const server = new Agent(config);
server.start();
server.setAdditionalServerInfo({
    agentPlatform: 'cli'
});
