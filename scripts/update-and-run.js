const Updater = require('../src/Updater');
const updateConfig = require('../config/update.json');

const updater = new Updater(updateConfig, process.platform);

updater.attemptUpdateThenInstall()
    .then(() => {
        console.log('Installed successfully.');
        console.log('Starting ProPoint Agent...');
        require('./run.js');
    })
    .catch(() => {
        console.log('Install failed. Cannot continue.');

        // We set a big timeout that doesn't do much just to prevent a rapid restart loop.
        // This gives more time to open the logs and see what's going on without cmd windows being
        // spammed all over the place.
        setTimeout(
            () => {
                process.exit();
            },
            15000
        );
    });
