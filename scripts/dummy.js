/**
 * If there are problems with a payment terminal, ProPoint will still be usable but will show an error
 * "There was a problem connecting to the payment terminal".
 * To suppress this error message run this script (using the DummyServer.bat in the root).
 * This will just accept connections on the same port as a payment terminal, causing the error message to not appear.
 */
const net = require('net');

const server = net.createServer(() => {
    console.log('New connection on 10000');
});

server.listen(10000, '127.0.0.1');
console.log('Fake server listening on 10000...');
