#!/bin/bash

# Run this on the dtapps.co.uk server after pulling the latest changes to generate the zip file
# that clients download.

cd "$(dirname "$0")" # cd to directory containing this script
cd ../ # go up one directory to the root of the project
parentdir="$(dirname `pwd`)" # create the zip in the directory containing the project root dir
zippath="${parentdir}/propoint-agent.zip" # path to the zip file
altzippath="${parentdir}/socket-proxy.zip" # path to the legacy zip file

rm -f $zippath
zip -r $zippath ./* --exclude=*.git* --exclude=node_modules* --exclude=.idea* --exclude=*.zip --exclude=*/.DS_Store --exclude=tests* --exclude=bitbucket* --exclude=test*

echo "Created ${zippath}"

cp $zippath $altzippath
echo "Created ${altzippath}"

