/**
 * If there are problems with a payment terminal, ProPoint will still be usable but will show an error
 * "There was a problem connecting to the payment terminal".
 * To suppress this error message run this script (using the DummyServer.bat in the root).
 * This will just accept connections on the same port as a payment terminal, causing the error message to not appear.
 */
const net = require('net');

const progressClients = [];

const server = net.createServer((client) => {
    console.log('25000 connection');

    // When data is received on the integration socket reply with "ready" on the progress socket.
    client.on(
        'data',
        (data) => {
            console.log('Received on integration socket', data.toString());
            console.log('Sending ready to ' + progressClients.length + ' progress sockets');
            for (let i = 0; i < progressClients.length; i++) {
                try {
                    progressClients[i].write('100,0,46,Ready,');
                } catch (e) {
                    console.log(e);
                }
            }
        }
    );
});

server.listen(25000, '127.0.0.1');
console.log('Fake server listening on 25000...');

const server2 = net.createServer((client) => {
    progressClients.push(client);

    client.on('end', () => {
        console.log('25001 disconnected');
        const index = progressClients.indexOf(client);
        if (index !== -1) {
            progressClients.splice(index, 1);
        }
    });

    console.log('25001 connection');
});
server2.listen(25001, '127.0.0.1');
console.log('Fake server listening on 25001...');
