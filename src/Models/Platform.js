class Platform {
    /**
     * @param {string} platform
     */
    constructor(platform) {
        this.platform = platform;
    }

    /**
     * @return {string}
     */
    getName() {
        return this.platform;
    }

    /**
     * @return {boolean}
     */
    isWindows() {
        return /^win/i.test(this.platform);
    }

    /**
     * @return {boolean}
     */
    isMac() {
        return /darwin/i.test(this.platform);
    }
}

module.exports = Platform;
