
const childProcess = require('child_process');
const fs = require('fs');
const http = require('http');
const https = require('https');
const rimraf = require('rimraf');
const AdmZip = require('adm-zip');

/**
 * @param {object} [config]
 * @param {string} [platform] Value of process.platform See https://nodejs.org/api/process.html#process_process_platform
 */
const Updater = function (config, platform) {
    /**
     * @type {boolean}
     */
    this.isWindows = platform ? /^win/.test(platform) : undefined;

    /**
     * Setting the library method here so it can be easily mocked for testing.
     */
    this.spawn = childProcess.spawn;

    /**
     * Username for HTTP authentication.
     *
     * @type {string}
     */
    this.username = '';

    /**
     * Password for HTTP authentication.
     *
     * @type {string}
     */
    this.password = '';

    /**
     * Where to unzip the downloaded zip file.
     *
     * @type {string}
     */
    this.unzipDestination = '';

    /**
     * Where to store the download zip file.
     *
     * @type {string}
     */
    this.zipPath = '';

    /**
     * 'http' or 'https'
     *
     * @type {string}
     */
    this.zipUrlProtocol = 'http';

    /**
     * Hostname to download the zip file from (e.g. www.mydomain.com)
     *
     * @type {string}
     */
    this.zipUrlHost = '';

    /**
     * Path to download the zip file from at the hostname (e.g. /filename.zip= http://www.mydomain.com/filename.zip)
     *
     * @type {string}
     */
    this.zipUrlPath = '';

    if (config) {
        for (const key in config) {
            if (config.hasOwnProperty(key) && this.hasOwnProperty(key)) {
                this[key] = config[key];
            }
        }

        // These paths in the config are relative to the root directory. Prefix them to make an absolute path.
        this.unzipDestination = __dirname + '/../' + this.unzipDestination;
        this.zipPath = __dirname + '/../' + this.zipPath;
    }
};

Updater.prototype = {
    /**
     * Download and save a file.
     * https://stackoverflow.com/a/22907134/710630
     *
     * @param {string} protocol
     * @param {string} host
     * @param {string} path
     * @param {string} destPath
     *
     * @return {Promise<string>}
     */
    download(protocol, host, path, destPath) {
        return new Promise((resolve, reject) => {
            const url = protocol + '://' + host + path;
            console.log('Downloading ' + url + '...');

            // See https://nodejs.org/api/http.html#http_http_request_options_callback for options.
            // 'port' and 'protocol' have been omitted because they will default to 80/http and 443/https depending
            // on which module is being used.
            const options = {
                host,
                path,
                headers: {
                    Authorization: 'Basic ' + Buffer.from(this.username + ':' + this.password).toString('base64')
                }
            };

            // Need to use the correct module depending on if the request is http or https.
            const httpModule = protocol === 'https' ? https : http;

            const file = fs.createWriteStream(destPath);
            httpModule.get(
                options,
                (response) => {
                    console.log('HTTP Response Code', response.statusCode);
                    if (response && response.statusCode && response.statusCode === 200) {
                        response.pipe(file);

                        file.on('finish', () => {
                            file.close(() => {
                                resolve(destPath);
                            }); // close() is async so resolve after close completes.
                        });
                    } else {
                        rimraf.sync(destPath); // Delete the file.
                        reject('Download Failed. HTTP Status Code: ' + response.statusCode);
                    }
                }
            )
                .on('error', (err) => {
                    fs.unlinkSync(destPath); // Delete the file.
                    reject('Download Failed. Request Error: ' + err);
                });
        });
    },

    /**
     * Extract the zip file at the specified path to the specified directory.
     *
     * @param {string} path
     * @param {string} dest
     *
     * @return {Promise<string>}
     */
    unzip(path, dest) {
        return new Promise((resolve, reject) => {
            console.log('Extracting ' + path + '...');
            try {
                const zip = new AdmZip(path);
                zip.extractAllTo(dest, true);
                setTimeout(() => resolve(dest), 2000);
            } catch (e) {
                reject('Failed to extract file. ' + e);
            }
        });
    },

    install() {
        return new Promise((resolve, reject) => {
            const npmCmd = this.isWindows ? 'npm.cmd' : 'npm';

            // The npm install command will pop up a separate command prompt (in Windows) momentarily.
            // The options are a fix to make it so this window will pop up, show its output, then close. Rather than
            // sticking around for the life of the process.
            const cmd = this.spawn(npmCmd, ['install'], {
                detached: true,
                shell: false
            });

            cmd.on('error', (err) => {
                console.log('Install Error:', err);
            });

            cmd.stdout.on('data', (data) => {
                console.log('Install Output:', data.toString());
            });

            cmd.stderr.on('data', (data) => {
                console.log('Install Error:', data.toString());
            });

            cmd.on('exit', (code) => {
                if (code.toString() === '0') {
                    resolve();
                } else {
                    reject('Install Failed: ' + code.toString());
                }
            });
        });
    },

    downloadAndUnzip() {
        return this.download(this.zipUrlProtocol, this.zipUrlHost, this.zipUrlPath, this.zipPath)
            .then((zipPath) => {
                console.log('Downloaded successfully to', zipPath);
                console.log('File size', fs.statSync(zipPath).size);

                return this.unzip(zipPath, this.unzipDestination);
            });
    },

    /**
     * Downloads the latest.zip, extracts it, then calls npm install.
     *
     * It continues to do the install even if the download/unzip fails (rejects) because we can continue using
     * the version we already have.
     *
     * If the install fails the promise returned here will reject because we can't run without that.
     *
     * @return {Promise<any>}
     */
    attemptUpdateThenInstall() {
        return new Promise((resolve, reject) => {
            const afterDownload = () => {
                this.install().then(resolve).catch(reject);
            };

            this.downloadAndUnzip()
                .then(() => {
                    console.log('Successfully downloaded latest version.');
                    afterDownload();
                })
                .catch(() => {
                    console.log('Download failed. Installing with existing version.');
                    afterDownload();
                });
        });
    }
};

module.exports = Updater;
