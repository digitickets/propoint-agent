const DeviceInfo = require('./DeviceInfo');
const express = require('express');
const FilesystemServer = require('./FilesystemServer');
const fs = require('fs');
const https = require('https');
const logFactory = require('./Logging/logFactory');
const packageJson = require('../package.json');
const path = require('path');
const Platform = require('./Models/Platform');
const socketIo = require('socket.io');
const TcpProxy = require('./TcpProxy');

/**
 * @param {object} [config]
 */
const Agent = function (config = {}) {
    /**
     * @type {Object}
     */
    this.config = config;

    /**
     * @type {boolean}
     */
    this.verbose = !config.hasOwnProperty('verbose') || !(config.verbose === false);

    /**
     * @type {ConsoleLogger}
     */
    this.log = logFactory('Agent', this.verbose);

    this.log.log('Starting ProPoint Agent Version', packageJson.version);

    /**
     * @type {Date}
     */
    this.startedAt = new Date();

    /**
     * @type {?Date}
     */
    this.lastDataAt = null;

    /**
     * How long should have been running for (in seconds) for before it considers restarting?
     *
     * @type {number}
     */
    this.maxUptime = config.hasOwnProperty('maxUptime') ? config.maxUptime : 43200; // 12 hours

    /**
     * If the server has been up for longer than this.maxUptime, and the current hour is this, it can restart.
     * Default is 2AM.
     *
     * @type {number}
     */
    this.restartHour = config.hasOwnProperty('restartHour') ? config.restartHour : 2;

    /**
     * How frequently (in seconds) to check if it is time to restart the server.
     * Changing is is only useful for unit testing.
     * Default is 1 minute.
     *
     * @type {number}
     */
    this.restartCheckIntervalMs = 60000;
    this.restartCheckInterval = null;

    /**
     * @type {number}
     */
    this.port = config.hasOwnProperty('port') ? config.port : 8443;

    this.defaultCertPath = path.resolve(__dirname, '..', 'ssl');

    if (config.hasOwnProperty('sslCertPath')) {
        this.sslCertPath = config.sslCertPath;
    } else {
        this.sslCertPath = this.defaultCertPath + '/localhost.dtapps.co.uk.crt';
    }

    if (config.hasOwnProperty('sslKeyPath')) {
        this.sslKeyPath = config.sslKeyPath;
    } else {
        this.sslKeyPath = this.defaultCertPath + '/localhost.dtapps.co.uk.key';
    }

    if (!this.sslCertPath || !this.sslKeyPath) {
        throw new Error('sslCertPath and sslKeyPath must be set in the config.');
    }

    try {
        console.log('Reading SSL certificate from', this.sslCertPath);
        this.sslCert = fs.readFileSync(this.sslCertPath);
    } catch (e) {
        throw new Error('Failed to read SSL cert: ' + e);
    }

    try {
        console.log('Reading SSL certificate key from', this.sslKeyPath);
        this.sslKey = fs.readFileSync(this.sslKeyPath);
    } catch (e) {
        throw new Error('Failed to read SSL key: ' + e);
    }

    /**
     * @type {Platform}
     */
    this.platform = new Platform(process.platform);

    this.tcpProxy = new TcpProxy(this);

    /**
     * @type {FilesystemServer}
     */
    this.filesystemServer = new FilesystemServer(this);

    /**
     * @type {?socketIo}
     */
    this.io = null;

    /**
     * @type {?https.Server}
     */
    this.httpServer = null;

    /**
     * @type {DeviceInfo}
     */
    this.deviceInfo = new DeviceInfo();

    /**
     * Any additional info to be added to the /version display page.
     *
     * @type {Object}
     */
    this.additionalServerInfo = {};
};

Agent.prototype = {

    start() {
        const self = this;

        // Start up http or https server
        const expressApp = express();

        this.httpServer = https.createServer(
            {
                cert: this.sslCert,
                key: this.sslKey
            },
            expressApp
        );

        this.io = socketIo(this.httpServer);

        this.startRestartTimer();

        // Start HTTP server.
        console.debug('Starting HTTP server on port', this.port);
        this.httpServer.listen(this.port, () => {
            this.log.log('Listening on *:' + self.port);
            this.onReady();
        });

        /**
         * GET /status
         */
        expressApp.get('/status', (req, res) => {
            const serverInfo = this.getStaticServerInfo();
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(serverInfo));
        });

        // For each WebSocket connection.
        this.io.on('connection', (webSocket) => {
            self.updateLastDataAt();
            this.log.debug(webSocket.id, 'WebSocket connected', webSocket.request.connection.remoteAddress);

            // Tell the WebSocket client some info about this server if they ask.
            webSocket.on('getServerInfo', (data, callback) => {
                this.getServerInfo().then((serverInfo) => {
                    // Send it as a callback.
                    callback(serverInfo);
                    // Also sent it as an emitted serverInfo event.
                    webSocket.emit('serverInfo', serverInfo);
                });
                return true;
            });

            /**
             * WebSocket sends a 'configure' message to configure some agent settings.
             */
            webSocket.on('configure', (data, callback) => {
                self.updateLastDataAt();
                this.log.debug(webSocket.id, 'WebSocket configure', data);

                if (data.hasOwnProperty('restartHour')) {
                    if (
                        parseInt(data.restartHour, 10)
                        || data.restartHour === 0
                        || data.restartHour === null
                    ) {
                        this.stopRestartTimer();
                        this.log.debug('Setting restart hour', self.restartHour);
                        self.restartHour = data.restartHour;
                        this.startRestartTimer();
                    }
                }

                // There is a test for calling configure without a callback but in Node 6.11.2 this else branch
                // still shows as uncovered. With Node 10.* it does not.
                /* istanbul ignore else */
                if (typeof callback === 'function') {
                    callback();
                }
            });

            /**
             * When the WebSocket is closed close the TCP socket.
             */
            webSocket.on('disconnect', () => {
                this.log.debug(webSocket.id, 'WebSocket disconnected', webSocket.request.connection.remoteAddress);
            });

            this.filesystemServer.onConnect(webSocket);
            self.tcpProxy.onConnect(webSocket);
        });
    },

    onReady() {
        console.log('\x1b[32m'); // Blank line and begin green text.
        console.log('****************************');
        console.log('* ProPoint Agent is ready. *');
        console.log('****************************');
        console.log('\x1b[0m'); // Blank line and reset to white.

        this.log.log('Started at', this.startedAt);
    },

    stop() {
        if (this.httpServer) {
            this.httpServer.close();
        }
        this.httpServer = null;

        if (this.io) {
            this.io.close();
        }
        this.io = null;
        if (this.restartCheckInterval !== null) {
            clearInterval(this.restartCheckInterval);
        }
    },

    /**
     * Returns how long the server has been running in seconds.
     *
     * @return {number}
     */
    getUptime() {
        return ((new Date()).getTime() - this.startedAt.getTime()) / 1000;
    },

    /**
     * Return the version of propoint-agent.
     *
     * @return {string}
     */
    getVersion() {
        return packageJson.version;
    },

    setAdditionalServerInfo(additionalServerInfo) {
        this.additionalServerInfo = additionalServerInfo;
    },

    getStaticServerInfo() {
        // Can't use "..." here because we need to support Node 6.
        return Object.assign(
            {
                time: new Date(),
                agentVersion: this.getVersion(),
                agentStartedAt: this.startedAt.toISOString(),
                agentDirectory: path.resolve(__dirname, '..')
            },
            this.additionalServerInfo
        );
    },

    /**
     * @return {Promise<{}>}
     */
    getServerInfo() {
        return new Promise((resolve) => {
            const defaultDeviceInfo = this.getStaticServerInfo();

            this.deviceInfo.getDeviceInfo()
                .then((deviceInfo) => resolve(Object.assign({}, defaultDeviceInfo, deviceInfo)))
                .catch((err) => {
                    console.log('Getting extended device info failed', err);
                    resolve(defaultDeviceInfo);
                });
        });
    },

    /**
     * Set the time the last data was sent to now.
     */
    updateLastDataAt() {
        this.lastDataAt = new Date();
    },

    stopRestartTimer() {
        if (this.restartCheckInterval !== null) {
            clearInterval(this.restartCheckInterval);
            this.restartCheckInterval = null;
        }
    },

    startRestartTimer() {
        if (!this.restartHour) {
            console.debug('[AutoRestart]', 'Restart hour is disabled.');
            return;
        }

        console.debug('[AutoRestart]', 'Starting interval to restart at hour', this.restartHour);
        this.restartCheckInterval = setInterval(
            () => {
                const isRestartTime = this.isRestartRequired();
                if (isRestartTime === true) {
                    this.log.log('[AutoRestart]', 'Restarting to apply updates.');
                    this.restart();
                }
            },
            this.restartCheckIntervalMs
        );
    },

    /**
     * Check if it's time to restart the server.
     * Returns true if it is, or a string reason otherwise.
     *
     * @return {string|boolean}
     */
    isRestartRequired() {
        if (this.restartHour === false) {
            return 'Auto restart disabled';
        }

        const now = new Date();

        // this.log.debug(
        //     'Checking for restart needed.',
        //     {
        //         'Uptime': this.getUptime(),
        //         'Current hour': now.getHours(),
        //         'Restart hour': this.restartHour,
        //         'Last data at': (this.lastDataAt ? now.getTime() - this.lastDataAt.getTime() : '')
        //     }
        // );

        if (this.getUptime() < this.maxUptime) {
            // Haven't been up long enough to need to restart.
            return 'Uptime too low';
        }

        if (this.lastDataAt && (now.getTime() - this.lastDataAt.getTime() < 600000)) {
            // There was data sent in the last 10 minutes. Don't restart.
            return 'Data sent recently';
        }

        // Check if it's the right hour to restart.
        if (now.getHours() === this.restartHour) {
            return true;
        }
        return 'Wrong hour';
    },

    restart() {
        this.log.log('ProPoint Agent is restarting...');
        process.exit(0);
    }
};

module.exports = Agent;
