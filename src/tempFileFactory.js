const tmp = require('tmp');
const fs = require('fs');

tmp.setGracefulCleanup();

const tempFileFactory = {
    /**
     * @param prefix
     * @param postfix
     * @return {{path: string, remove: fileCallback}}
     */
    makeFile(prefix, postfix) {
        const tempFile = tmp.fileSync({
            mode: '0666',
            prefix,
            postfix,
            discardDescriptor: true
        });
        return {
            path: tempFile.name,
            remove: () => {
                /* istanbul ignore else */
                if (fs.existsSync(tempFile.name)) {
                    console.log('Deleting temp file ' + tempFile.name);
                    tempFile.removeCallback();
                }
            }
        };
    },

    /**
     * @param prefix
     * @return {{path: string, remove: fileCallback}}
     */
    makeDirectory(prefix) {
        const tempDir = tmp.dirSync({
            mode: '0777',
            prefix,
            unsafeCleanup: true,
            discardDescriptor: true
        });
        return {
            path: tempDir.name,
            remove: () => {
                /* istanbul ignore else */
                if (fs.existsSync(tempDir.name)) {
                    console.log('Deleting temp dir ' + tempDir.name);
                    tempDir.removeCallback();
                }
            }
        };
    }
};

module.exports = tempFileFactory;
