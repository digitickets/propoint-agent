const fs = require('fs');
const path = require('path');
const logFactory = require('./Logging/logFactory');

class Filesystem {
    /**
     * @param {Platform} platform
     * @param {{}} config
     */
    constructor(
        platform,
        config = {}
    ) {
        this.log = logFactory('Filesystem');
        this.platform = platform;

        if (config.hasOwnProperty('dataDir')) {
            this.dataDir = config.dataDir;
        } else {
            this.dataDir = Filesystem.getDefaultDataDir(this.platform);
        }

        this.log.debug('Data Directory:', this.dataDir);
        this.ensureDataDirExists();

        this.pathSeparator = path.sep;
        this.log.debug('Path Separator:', this.pathSeparator);
    }

    /**
     * @param {Platform} platform
     *
     * @returns {string}
     */
    static getDefaultDataDir(platform) {
        if (platform.isWindows()) {
            return Filesystem.defaultWindowsDataDir;
        }

        return Filesystem.defaultUnixDataDir;
    }

    /**
     * @param {{path: string}} data
     * @param {boolean} allowOutsideDataDir
     *
     * @return {string}
     */
    validatePath(data, allowOutsideDataDir) {
        if (!data.path) {
            throw new Error('path must be specified.');
        }

        if (!allowOutsideDataDir && !data.path.startsWith(this.dataDir)) {
            // Prepend the data directory to the path.
            data.path = path.join(this.dataDir, data.path);
        }

        const normalizedPath = this.normalizeFilename(data.path);

        const resolvedPath = path.resolve(normalizedPath);

        // Use the path module to ensure the path is inside the data directory.
        if (!allowOutsideDataDir && !resolvedPath.startsWith(this.dataDir)) {
            throw new Error('Invalid path.');
        }

        return resolvedPath;
    }

    /**
     * @param {string} filename
     *
     * @return {string}
     */
    normalizeFilename(filename) {
        // Fix all kinds of directory separator to the correct one for this platform.
        const directorySeparators = [
            new RegExp('\\\\', 'g'), // Windows
            new RegExp('/', 'g'), // Unix
        ];
        directorySeparators.forEach((regex) => {
            filename = filename.replace(regex, this.pathSeparator);
        });

        return filename;
    }

    /**
     * @param {{path: string, data: string}} data
     * @param {boolean} allowOutsideDataDir
     *
     * @return {Promise<{path: string, success:boolean}>}
     */
    async writeFile(data, allowOutsideDataDir) {
        const filePath = this.validatePath(data, allowOutsideDataDir);

        return new Promise((resolve, reject) => {
            if (!data.hasOwnProperty('data')) {
                reject(new Error('data must be specified (but can be empty).'));
                return;
            }

            this.log.debug(`Writing file ${filePath}`);

            fs.writeFile(
                filePath,
                data.data,
                {},
                (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve({ path: data.path, success: true });
                    }
                }
            );
        });
    }

    /**
     * @param {{path: string, data: string}} data
     * @param {boolean} allowOutsideDataDir
     *
     * @return {Promise<{path: string, success:boolean}>}
     */
    async appendFile(data, allowOutsideDataDir) {
        const filePath = this.validatePath(data, allowOutsideDataDir);

        return new Promise((resolve, reject) => {
            if (!data.hasOwnProperty('data')) {
                reject(new Error('data must be specified (but can be empty).'));
                return;
            }

            this.log.debug(`Appending to file ${filePath}`);

            fs.appendFile(
                filePath,
                data.data,
                {},
                (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve({ path: data.path, success: true });
                    }
                }
            );
        });
    }

    /**
     * @param {{path: string}} data
     * @param {boolean} allowOutsideDataDir
     *
     * @return {Promise<{path: string, success: boolean, data: string}>}
     */
    async readFile(data, allowOutsideDataDir) {
        const filePath = this.validatePath(data, allowOutsideDataDir);

        return new Promise((resolve, reject) => {
            this.log.debug(`Reading file ${filePath}`);

            fs.readFile(
                filePath,
                {},
                (error, result) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve({ path: data.path, success: true, data: result.toString() });
                    }
                }
            );
        });
    }

    ensureDataDirExists() {
        if (fs.existsSync(this.dataDir)) {
            this.log.debug('Data directory exists:', this.dataDir);
        } else {
            this.log.debug('Creating data directory:', this.dataDir);
            fs.mkdirSync(this.dataDir);
        }
    }
}

Filesystem.defaultWindowsDataDir = 'C:\\propoint-data';
Filesystem.defaultUnixDataDir = process.env.HOME + '/propoint-data';

module.exports = Filesystem;
