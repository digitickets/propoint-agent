const si = require('systeminformation');
const request = require('request');

const DeviceInfo = function () {
    // Set here for easy mocking.
    this.si = si;
    this.request = request;

    /**
     * URL to a page that returns the IP address making the request alone.
     *
     * @type {string}
     */
    this.externalIpUrl = 'https://dtapps.co.uk/ip.php';
};

DeviceInfo.prototype = {
    getDeviceInfo() {
        const results = {
            machineIdentifier: null,
            systemModel: null,
            computerName: null,
            internalIp: null,
            externalIp: null,
            nodeVersion: process.versions.node
        };

        /**
         * @type {Promise[]}
         */
        const promises = [];

        promises.push(
            this.getMachineIdentifier()
                .then((machineIdentifier) => {
                    results.machineIdentifier = machineIdentifier;
                })
                .catch(() => console.error('Failed to get machineIdentifier.'))
        );

        promises.push(
            this.si.osInfo()
                .then((osInfo) => {
                    results.computerName = osInfo.hostname;
                })
                .catch(() => console.error('Failed to get computerName.'))
        );

        promises.push(
            this.getSystemModel()
                .then((result) => {
                    results.systemModel = result;
                })
                .catch(() => console.error('Failed to get systemModel.'))
        );

        promises.push(
            this.getExternalIp()
                .then((result) => {
                    results.externalIp = result;
                })
                .catch(() => console.error('Failed to get externalIp.'))
        );

        promises.push(
            this.getInternalIp()
                .then((result) => {
                    results.internalIp = result;
                })
                .catch(() => console.error('Failed to get internalIp.'))
        );

        return Promise.all(promises)
            .then(() => results);
    },

    getSystemModel() {
        /**
         * @type {Promise[]}
         */
        const promises = [];

        let system = null;
        promises.push(this.si.system().then((result) => {
            system = result;
        }));

        let cpu = null;
        promises.push(this.si.cpu().then((result) => {
            cpu = result;
        }));

        let mem = null;
        promises.push(this.si.mem().then((result) => {
            mem = result;
        }));

        return Promise.all(promises)
            .then(() => {
                let str = '';

                if (system && system.manufacturer) {
                    str += ' ' + system.manufacturer;
                }

                if (system && system.model) {
                    str += ' ' + system.model;
                }

                let cpuStr = '';
                if (cpu && cpu.manufacturer) {
                    cpuStr += cpu.manufacturer;
                }
                if (cpu && cpu.brand) {
                    cpuStr += ' ' + cpu.brand;
                }
                if (cpu && cpu.speed) {
                    cpuStr += ' ' + cpu.speed + 'GHz';
                }
                if (cpuStr) {
                    str += ` (${cpuStr.trim()} CPU)`;
                }

                if (mem && mem.total) {
                    const memInGb = Math.round((mem.total / 1024 / 1024 / 1024) * 100) / 100;
                    str += ` (${memInGb}GB RAM)`;
                }

                return str.trim();
            });
    },

    getMachineIdentifier() {
        let osUuid;
        let systemSerial;

        let promises = [];

        promises.push(
            this.si.system()
                .then((system) => {
                    systemSerial = system.serial;
                })
        );

        promises.push(
            this.si.uuid()
                .then((uuid) => {
                    osUuid = uuid.os;
                })
        );

        return Promise.all(promises)
            .then(() => {
                return systemSerial + '-' + osUuid;
            });
    },

    getExternalIp() {
        return new Promise((resolve, reject) => {
            this.request.get(this.externalIpUrl, {}, (err, response, body) => {
                if (err || !body) {
                    reject();
                    return;
                }
                try {
                    const results = JSON.parse(body);
                    if (results.ip) {
                        resolve(results.ip);
                    } else {
                        reject();
                    }
                } catch (e) {
                    reject();
                }
            });
        });
    },

    async getInternalIp() {
        /**
         * @type {Systeminformation.NetworkInterfacesData[]}
         */
        const interfaces = await this.si.networkInterfaces();

        const defaultInterfaceName = await this.si.networkInterfaceDefault();
        if (!defaultInterfaceName) {
            throw new Error('No default network interface name.');
        }

        const defaultInterface = interfaces.find((i) => i.iface === defaultInterfaceName);
        if (!defaultInterface) {
            throw new Error(`Unable to get info about default network interface (${defaultInterfaceName}).`);
        }

        return defaultInterface.ip4 || defaultInterface.ip6;
    }
};

module.exports = DeviceInfo;
