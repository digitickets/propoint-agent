const Filesystem = require('./Filesystem');
const logFactory = require('./Logging/logFactory');
const path = require('path');
const promiseToCallback = require('./Functions/promiseToCallback');

class FilesystemServer {
    /**
     * @param {Agent} agent
     */
    constructor(
        agent
    ) {
        this.agent = agent;
        this.log = logFactory('FilesystemServer');
        this.filesystem = new Filesystem(
            agent.platform,
            agent.config
        );
    }

    onConnect(webSocket) {
        // The ability to write arbitrary files is disabled until we can add some security.
        // See https://digitickets.atlassian.net/browse/PROP-1475
        // webSocket.on('writeFile', (data, callback) => {
        //     this.log.debug('writeFile', data);
        //     return promiseToCallback(
        //         this.filesystem.writeFile(data),
        //         callback
        //     );
        // });

        webSocket.on('writeFileToDataDir', (data, callback) => {
            this.log.debug('writeFileToDataDir', data);
            return promiseToCallback(
                this.filesystem.writeFile(data, false),
                callback
            );
        });

        // The ability to write arbitrary files is disabled until we can add some security.
        // See https://digitickets.atlassian.net/browse/PROP-1475
        // webSocket.on('appendFile', (data, callback) => {
        //     this.log.debug('appendFile', data);
        //     return promiseToCallback(
        //         this.filesystem.appendFile(data),
        //         callback
        //     );
        // });

        webSocket.on('appendFileToDataDir', (data, callback) => {
            this.log.debug('appendFileToDataDir', data);
            return promiseToCallback(
                this.filesystem.appendFile(data, false),
                callback
            );
        });

        // The ability to read arbitrary files is disabled until we can add some security.
        // See https://digitickets.atlassian.net/browse/PROP-1475
        // webSocket.on('readFile', (data, callback) => {
        //     this.log.debug('readFile', data);
        //     return promiseToCallback(
        //         this.filesystem.readFile(data),
        //         callback
        //     );
        // });

        webSocket.on('readFileFromDataDir', (data, callback) => {
            this.log.debug('readFileFromDataDir', data);
            return promiseToCallback(
                this.filesystem.readFile(data, false),
                callback
            );
        });
    }
}

module.exports = FilesystemServer;
