/**
 * Accepts a promise and calls the callback function when it resolves or rejects.
 * If it resolves the callback is called with { success: true } and the rest of the data.
 * If it rejects the callback is called with { error: 'the error message' }.
 * Useful for using a promise in response to a websocket event.
 *
 * @param {Promise} promise
 * @param {Function} callback
 */
const promiseToCallback = (promise, callback) => {
    promise
        .then((result) => {
            if (typeof result === 'string') {
                callback({ success: true, result });
            } else {
                callback(Object.assign(
                    { success: true },
                    result
                ));
            }
        })
        .catch((error) => callback({ error: error.message }));

    // Returning true in a 'websocket.on' means we are going to call the callback function at some point later.
    // Returning true here means you can just 'return promiseToCallback()' in the '.on' handler.
    return true;
};

module.exports = promiseToCallback;
