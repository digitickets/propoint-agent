const logFactory = require('./Logging/logFactory');
const net = require('net');

/**
 * @param {Agent} agent
 */
const TcpProxy = function (agent) {
    this.agent = agent;

    /**
     * @type {ConsoleLogger}
     */
    this.log = logFactory('TcpProxy', this.verbose);
};

TcpProxy.prototype = {
    onConnect(webSocket) {
        const self = this;
        const agent = this.agent;

        this.log.debug('Setting up TcpProxy handlers.');

        /**
         * The TCP sockets this WebSocket connected to.
         *
         * @type {Object.<string,net.Socket>}
         */
        const tcpSockets = {};

        /**
         * Info about the TCP sockets this WebSocket connected to.
         *
         * @type {Object.<string,{host: string, port: number}>}
         */
        const tcpSocketInfo = {};

        /**
         * @param {string} socketName
         * @returns {net.Socket}
         */
        const getSocketByName = (socketName) => {
            const tcpSocket = tcpSockets[socketName];

            if (!tcpSocket) {
                throw new Error('That socket name does not exist.');
            }

            return tcpSocket;
        };

        /**
         * @param {string} socketName
         */
        const disconnectTcpSocketByName = (socketName) => {
            const tcpSocket = getSocketByName(socketName);

            this.log.log('Disconnecting TCP socket', socketName);

            // See https://stackoverflow.com/a/52927597/710630
            tcpSocket.end();
            tcpSocket.destroy();

            // It's safe to call delete even if the key doesn't exist - it doesn't break.
            delete tcpSockets[socketName];
            delete tcpSocketInfo[socketName];
        };

        /**
         * When a WebSocket sends 'sendData', forward it to the TCP socket.
         */
        webSocket.on('sendTcpData', (data, callback) => {
            agent.updateLastDataAt();
            this.log.debug(webSocket.id, 'sendTcpData', data);

            if (typeof callback !== 'function') {
                // Ensure callback is always a function.
                callback = () => {};
            }

            if (!data.socketName) {
                callback({
                    error: 'socketName must be specified.'
                });
                return;
            }

            let tcpSocket;
            try {
                tcpSocket = getSocketByName(data.socketName);
            } catch (e) {
                callback({
                    error: e.message
                });
                return;
            }

            try {
                this.log.debug('<<<', 'Sending data to ' + tcpSocketInfo[data.socketName].host + ':' + tcpSocketInfo[data.socketName].port);
                console.debug(data.data);
            } catch (e) {

            }

            tcpSocket.write(
                data.data,
                (response) => {
                    callback(response);
                }
            );
        });

        webSocket.on('disconnectTcpSocket', (data, callback) => {
            this.log.debug(webSocket.id, 'disconnectTcpSocket', data);

            if (typeof callback !== 'function') {
                // Ensure callback is always a function.
                callback = () => {};
            }

            if (!data.socketName) {
                callback({
                    error: 'socketName must be specified.'
                });
                return;
            }

            try {
                disconnectTcpSocketByName(data.socketName);
                // Timeout so that the TCP server has time to actually do the disconnection.
                setTimeout(() => {
                    callback(true);
                }, 100);
            } catch (e) {
                callback({
                    error: e.message
                });
            }
        });

        /**
         * When the WebSocket is closed close the TCP socket.
         */
        webSocket.on('disconnect', () => {
            // Disconnect all this WebSocket's TCP sockets.
            Object.keys(tcpSockets).forEach((socketName) => {
                try {
                    disconnectTcpSocketByName(socketName);
                } catch (e) {
                }
            });
        });

        /**
         * WebSocket sends a 'connect' message connect to a new TCP socket.
         */
        webSocket.on('connectTcpSocket', (data, callback) => {
            agent.updateLastDataAt();
            this.log.debug(webSocket.id, 'connectTcpSocket', data);

            if (typeof callback !== 'function') {
                this.log.error('connectTcpSocket received without a callback');
                return;
            }

            const requiredParams = ['host', 'port', 'socketName'];

            for (let i = 0; i < requiredParams.length; i++) {
                if (!data.hasOwnProperty(requiredParams[i]) || !data[requiredParams[i]]) {
                    callback({ error: requiredParams[i] + ' must be specified.' });
                    return false;
                }
            }

            const socketName = data.socketName;

            if (tcpSockets.hasOwnProperty(socketName)) {
                callback({
                    error: 'That socket name is already in use.'
                });
                return false;
            }

            try {
                const host = data.host;
                const port = data.port;

                tcpSockets[socketName] = self.createTcpSocket(
                    host,
                    port,
                    data.encoding,
                    (result, encoding) => {
                        this.log.debug('TCP Socket Connected (' + host + ':' + port + ')');

                        // Connected
                        callback({
                            socketName,
                            encoding,
                            connected: true
                        });
                    },
                    () => {
                        this.log.debug('tcpSocketClosed (' + host + ':' + port + ')');

                        // Closed
                        webSocket.emit('tcpSocketClosed', {
                            socketName
                        });
                        /* istanbul ignore else */
                        if (tcpSockets.hasOwnProperty(socketName)) {
                            delete tcpSockets[socketName];
                        }
                    },
                    (data) => {
                        agent.updateLastDataAt();
                        this.log.debug('>>>', 'Received data from ' + host + ':' + port);
                        console.debug(data);

                        // Data received
                        webSocket.emit('tcpSocketData', {
                            socketName,
                            data
                        });
                    },
                    (error) => {
                        // Error
                        this.log.debug('tcpSocketError (' + host + ':' + port + ')', data);

                        webSocket.emit('tcpSocketError', {
                            socketName,
                            error
                        });
                    }
                );

                tcpSocketInfo[socketName] = {
                    host: data.host,
                    port: data.port
                };
            } catch (e) {
                console.log(webSocket.id, 'TCP Socket connection exception', e.toString());
                webSocket.emit('tcpSocketError',
                    {
                        error: e.toString()
                    });
            }

            return true;
        });
    },

    /**
     * @param {string} host
     * @param {number} port
     * @param {string|null} encoding
     * @param {function} onConnect
     * @param {function} onClose
     * @param {function} onError
     * @param {function} onData
     *
     * @return {net.Socket}
     */
    createTcpSocket(
        host,
        port,
        encoding,
        onConnect,
        onClose,
        onData,
        onError
    ) {
        // Create TCP socket
        const tcpSocket = new net.Socket();

        // Handle TCP socket errors.
        tcpSocket.on('error', (e) => {
            onError(e.toString());
        });

        // Handle TCP socket data.
        tcpSocket.on('data', (data) => {
            onData(data);
        });

        // Handle TCP socket closing.
        tcpSocket.on('close', () => {
            onClose();
        });

        // Set TCP socket encoding
        if (!encoding) {
            encoding = 'latin1';
        }
        tcpSocket.setEncoding(encoding);

        // Connect to TCP socket
        tcpSocket.connect(port, host, (result) => {
            onConnect(result, encoding);
        });

        return tcpSocket;
    }
};

module.exports = TcpProxy;
