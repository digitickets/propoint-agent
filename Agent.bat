:: Change directory to the path of this batch file
cd %~dp0

:: Install dependencies
:: See https://github.com/npm/npm/issues/2938 for why it has 'call' in front
call npm install --only=prod

:: Start pm2 to keep server alive
:: The --update-env flag tells pm2 to re-read the pm2.json file to check for changes.
call node_modules\.bin\pm2 start .\config\pm2.json --update-env
